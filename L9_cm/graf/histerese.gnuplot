reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'histerese.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 0.1
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 0.1
set style line 3 lc rgb '#FF9F00' dt 1 lw 1.5 pt 6 ps 0.1
set style line 4 lc rgb '#D2D900' dt 1 lw 1.5 pt 6 ps 0.5

set yrange[-1.5:2]
set xrange[-300:300]

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key top left Left title 'Legenda' nobox width -5

#axis labels
set xlabel '$H~(Am^{-1})$'
set ylabel '$B~(T)$'

lina(x)=a*x+b
linb(x)=c*x+d
a=10;
b=-10;
c=10;
d=-10;
fit lina(x) "reta_direita.txt"using   (($1*2.307)*1298/(0.16*60)):($2*100*10**(3)*10**(-6)/(45*7.5*10**(-4))) via a,b
fit linb(x) "reta_esquerda.txt" using ($1*2.307*1298/(0.16*60)):($2*100*10**(3)*10**(-6)/(45*7.5*10**(-4))) via c,d

plot 'histerese.txt' u ($1*2.307*1298/(0.16*60)):($2*100*10**(3)*10**(-6)/(45*7.5*10**(-4))) title  'Pontos experimentais' ls 2, \
     'reta_direita.txt' u ($1*2.307*1298/(0.16*60)):($2*100*10**(3)*10**(-6)/(45*7.5*10**(-4))) notitle ls 4, \
     'reta_esquerda.txt' u ($1*2.307*1298/(0.16*60)):($2*100*10**(3)*10**(-6)/(45*7.5*10**(-4))) notitle ls 3, \
     lina(x) title sprintf('$B=(%1.3f\pm %1.3f)H+(%1.2f\pm %1.2f)$',a,a_err,b,b_err) ls 4, \
     linb(x) title sprintf('$B=(%1.3f\pm %1.3f)H+(%1.2f\pm %1.2f)$',c,c_err,d,d_err) ls 3

     
