reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'sol_2_5.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF9F00' dt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key top right title 'Legenda' nobox width -10.5

#axis labels
set xlabel '$d~(cm)$'
set ylabel '$B~(mT)$'
#set xtics 1
#set xrange [50:10000]
#set yrange [0:40]
set bars 2

#Teorico
uo=1.25663706143592E-6
i=1.008
R=0.019
l=0.186
r(x)=sqrt(R*R+(x*0.01+l/2)**2)
rr(x)=sqrt(R*R+(-x*0.01+l/2)**2)
B(x)=1300*uo/2*1000*i*((x*0.01+l/2)/r(x)+(-x*0.01+l/2)/rr(x))

plot 'sol_2.5.txt' u 1:2:(0.1):3 title  'normal'  w xyerrorbars   ls 2,\
      B(x) title 'teorico' ls 2
