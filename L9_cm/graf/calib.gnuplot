reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'calib.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#111111' dt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key bottom right Right title 'Legenda' nobox width -4

#axis labels
set xlabel '$V_{sonda}~(mV)$'
set ylabel '$B~(mT)$'
#set xtics 1
#set xrange [50:10000]
#set yrange [0:90]
set bars 2

#fit para a fase
lina(x)=a*x+b
a=10;
b=1;
fit lina(x) "calib.txt" using 1:2:(0.1):3 xyerrors via a,b


plot 'calib.txt' u 1:2:(0.1):3 title  'Dados'  w xyerrorbars   ls 2,\
    lina(x) title sprintf('$B=(%1.4f\pm %0.4f)V_{sonda} + %1.4f\pm %0.4f$',a,a_err, b, b_err) ls 2
