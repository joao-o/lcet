reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'ferr.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF9F00' lt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key top right title 'Legenda' nobox width -10.5

#axis labels
set xlabel '$d~(cm)$'
set ylabel '$B~(mT)$'
#set xtics 1
#set xrange [50:10000]
#set yrange [0:40]
set bars 2


#Teorico
uo=1.25663706143592E-6
R=0.0325
k=7000
l=0.15
r(x)=sqrt((R)**2+(x*0.01+l)**2)
rr(x)=sqrt((R)**2+(-x*0.01)**2)
B(x)=uo/2*k*1000*((x*0.01+l)/r(x)+(-x*0.01)/rr(x))
kk=7000
a=0.01
ra(x)=sqrt((R)**2+(x*0.01+l-a)**2)
rra(x)=sqrt((R)**2+(-x*0.01-a)**2)
Ba(x)=uo/2*kk*1000*((x*0.01+l-a)/r(x)+(-x*0.01-a)/rr(x))

fit B(x) 'ferr.txt' using 1:2:(0.1):3 xyerrors via k
fit Ba(x) 'ferr.txt' using 1:2:(0.1):3 xyerrors via kk,a

plot 'ferr.txt' u 1:2:(0.1):3 title  'tangencial'  w xyerrorbars   ls 2,\
     B(x) title sprintf('$n(i+i")=(%1.0f\pm %1.0f) $',k,k_err) ls 2,\
     Ba(x) title sprintf('$n(i+i")=(%1.0f\pm %1.0f)a=(%1.2f\pm %1.2f)cm$',kk,kk_err, a*10**2, a_err*10**2) ls 3,\
