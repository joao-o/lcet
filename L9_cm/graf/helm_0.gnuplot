reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'helm_0.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF9F00' lt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key top right title 'Legenda' nobox width -10.5

#axis labels
set xlabel '$d~(cm)$'
set ylabel '$B~(mT)$'
#set xtics 1
#set xrange [50:10000]
#set yrange [0:40]
set bars 2

#Teorico
R=0.068
B(x)=9.2971*10**(-4)*(1/((0.004624+(-R/2+x*0.01)**2)**(1.5))+1/((0.004624+(R/2+x*0.01)**2)**(1.5)))

plot 'helm_0_n.txt' u 1:2:(0.1):3 title  'tangencial'  w xyerrorbars   ls 2,\
     B(x) title 'teorico' ls 2,\
     'helm_0_t.txt' u 1:2:(0.1):3 title  'normal'  w xyerrorbars   ls 3,\
     0 title 'teorico' ls 3
