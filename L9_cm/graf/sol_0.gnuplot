reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'sol_0.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF9F00' lt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key bottom left title 'Legenda' nobox width -10.5

#axis labels
set xlabel '$d~(cm)$'
set ylabel '$B~(mT)$'
#set xtics 1
#set xrange [50:10000]
#set yrange [0:40]
set bars 2



uo=1.25663706143592E-6
R=0.038/2
l=0.186
r(x)=sqrt((R)**2+(x*0.01+l/2)**2)
rr(x)=sqrt((R)**2+(-x*0.01+l/2)**2)
B(x)=uo/2*1300*1000*((x*0.01+l/2)/r(x)+(-x*0.01+l/2)/rr(x))

plot 'sol_0.txt' u 1:2:(0.1):3 title  'eixo central'  w xyerrorbars   ls 2,\
     'sol_2.5.txt' u 1:2:(0.1):3 title  'eixo distante 2.5 cm do eixo central'  w xyerrorbars   ls 3,\
     B(x) notitle ls 2
