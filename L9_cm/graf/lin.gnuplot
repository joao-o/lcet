reset

# epslatex
set terminal epslatex size 16cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'lin.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#1356BC' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#111111' dt 2 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12
#set grid mxtics ytics xtics ls 12


#Legenda
set key bottom right Left title 'Legenda' nobox width -10.5

#axis labels
set xlabel '$I_{espira}~(A)$'
set ylabel '$V_{sonda}~(V)$'
#set xtics 1
#set xrange [50:10000]
set yrange [0:40]
set bars 2

#fit para a fase
lina(x)=a*x+b
a=10;
b=-10;
fit lina(x) "lin.txt" using 1:2:(0.01):(0.2) xyerrors via a,b


plot 'lin.txt' u 1:2:(0.01):(0.2) title  'Dados'  w xyerrorbars   ls 2,\
    lina(x) notitle ls 2
