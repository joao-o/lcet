% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  2.5
cct_init

Origin: Here

source(up_ 0.7,S);variable;
line right 1 
T1:transformer(down_ 0.7) with .P1 at Here
move to T1.S1
resistor(right_ 0.7);rlabel(,"$R_s$",);
{
  dot
  capacitor(down_ 0.7);rlabel(,"$C$",);
  dot
}
line right 0.5
source(down_ 0.7,"$Y_2$");
line to T1.S2
move to T1.P2
line  left 0.2
{
  dot
  line up 0.4
  source(left_ 0.6,"$Y_1$");
  line down 0.4
  dot
}
resistor(left_ 0.6);rlabel(,"$R_p$",);

line to Origin
.PE
