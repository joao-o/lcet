reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'U_0_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5


#Title
#set title "Linearidade de grandezas em fun\\\c{c}\\\{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
#set grid back ls 12 pi -5
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12

#set mxtics 2
#set mytics 10

#Legenda
set key top left title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'
set ylabel 'I/ mA' #rotate by  90 center 

set tics scale 2

set bars 2 #espessura das barras de erro

#fits
max(x)=a+b*x
fit max(x) 'U_0_cos.txt' u 3:5 via a,b

set label 1 '$I=(8.7\pm 0.2)\cos(\alpha)+(-1.1\pm0.2)$' at 0.60,5.5 rotate by 49 center tc ls 1

set xrange [0.1:1.4]
set yrange [2:9]
plot 'U_0_cos.txt' u 3:5:4:6 title "$I(U=0)$" w xyerrorbars ls 2,\
	 max(x) notitle ls 1

