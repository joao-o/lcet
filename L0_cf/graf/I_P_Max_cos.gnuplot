reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'I_P_Max_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5


#Title
#set title "Linearidade de grandezas em fun\\\c{c}\\\~{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\~{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12


#set mxtics 2
#set mytics 10

#Legenda
set key top left title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

#fit
max(x)=a+b*x
fit max(x) 'I_P_Max_cos.txt' u 3:5 via a,b

set label 1 '$I=(6.6\pm 0.2)\cos(\alpha)+(-0.8\pm 0.2)$' at 0.8,5 rotate by 52 center tc ls 1

#4
######
set xrange [0.1:1.4]
set yrange [2:7]
set ylabel 'I/ mA' #rotate by  90 center 
plot 'I_P_Max_cos.txt' u 3:5:4:6 title "$I(P=P_{Max})$" w xyerrorbars ls 2,\
	 max(x) notitle ls 1, \
