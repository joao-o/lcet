reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'logP_logD.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 1.5 pt 8 ps 3

#para quando aprender a utilizar uma coluna do ficheiro para selecionar o tipo de ponto
set style line 9 lc rgb '#ff0000' lt 1 lw 1.5 pt 11 ps 1
set style line 10 lc rgb '#0000ff' lt 1 lw 1.5 pt 11 ps 1
set style line 11 lc rgb '#ffa500' lt 1 lw 1.5 pt 11 ps 1
set style line 12 lc rgb '#9400d3' lt 1 lw 1.5 pt 11 ps 1
set style line 13 lc rgb '#00ffa5' lt 1 lw 1.5 pt 11 ps 1
set style line 14 lc rgb '#800000' lt 1 lw 1.5 pt 11 ps 1
unset key

#Title
#set title "Pot\\\^{e}ncia do Painel em função da distância"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12

#set mxtics 2
#set mytics 10

#set xrange [0:1800]
#set xtics 0,200,1800
#set yrange [0:30]
#set ytics 0,5,30
#set logscale y
#set yrange [1e7:1e22]

#Legenda
set key top right title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'log(P)'
set ylabel 'log(D)' #rotate by  90 center 

set tics scale 2

set bars 2 #espessura das barras de erro

#fits
max(x)=a+b*x
a=-3;b=-2;
fit max(x) 'log(P)_log(D).txt' via a,b

set label 1 '$log(P)=(-2.1\pm 0.1)log(D)+(-3.15\pm 0.08)$' at -0.7,-1.9 rotate by -33 center tc ls 1

plot 'log(P)_log(D).txt'   u 1:2:3:4 title "m\'{a}ximos" w xyerrorbars ls 1, \
     max(x) notitle ls 1
