reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_R_D_2.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 1.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 4 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 1.5
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 1.5
set style line 6 lc rgb '#00ff00' lt 1 lw 1.5 pt 6 ps 1.5
set style line 7 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 8 lc rgb '#000f0f' lt 1 lw 1.5 pt 8 ps 3
unset key

#Title
#set title "Pot\\\^{e}ncia do Painel em função da distância"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb '#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12

#set xrange [0:2000]
#set xtics 0,200,1800
#set yrange [0:30]
#set ytics 0,5,30


#Legenda
set key top right title 'Legenda' box 5

#axis labels
set xlabel 'R/ $\Omega$'
set ylabel 'P/ mW' #rotate by  90 center 

set tics scale 2

set bars 2 #espessura das barras de erro


#set label 1 '$1.55\pm0.05$' at 0.8,8 tc ls 1
#set label 2 '$1.49\pm0.05$' at 0.6,6.35 tc ls 2
#set label 3 '$1.61\pm0.04$' at 0.5,5.5 tc ls 3
#set label 4 '$1.86\pm0.06$' at 0.3,4 tc ls 4
#set label 5 '$2.24\pm0.09$' at 0.1,2.1 tc ls 5
#set label 6 '$1.86\pm0.06$' at 0.3,4 tc ls 4
set label 1 '$(571,20.2)$' at 571,5 ls 1

#fits
max(x)=a+b/(x+c)
a=-1.1;b=6100;c=141;
fit max(x) 'P_R_cm_max.txt' via a,b,c

plot 'P_R_30cm.txt' u 1:2:3:4 title "$15~cm$" w xyerrorbars ls 7, \
     'P_R_18cm.txt' u 1:2:3:4 title "$18~cm$" w xyerrorbars ls 2, \
     'P_R_21cm.txt' u 1:2:3:4 title "$21~cm$" w xyerrorbars ls 3, \
     'P_R_24cm.txt' u 1:2:3:4 title "$24~cm$" w xyerrorbars ls 4, \
     'P_R_25cm.txt' u 1:2:3:4 title "$25~cm$" w xyerrorbars ls 5, \
     'P_R_27cm.txt' u 1:2:3:4 title "$27~cm$" w xyerrorbars ls 6, \
     'P_R_15cm.txt' u 1:2:3:4 title "$30~cm$" w xyerrorbars ls 1, \
     'P_R_cm_max.txt' u 1:2:3:4 title "maximos" w xyerrorbars ls 8
