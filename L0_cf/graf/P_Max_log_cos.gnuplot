reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_Max_log_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5


#para quando aprender a utilizar uma coluna do ficheiro para selecionar o tipo de ponto
set style line 9 lc rgb '#ff0000' lt 1 lw 1.5 pt 11 ps 1
#set style line 10 lc rgb '#0000ff' lt 1 lw 1.5 pt 11 ps 1
#set style line 11 lc rgb '#ffa500' lt 1 lw 1.5 pt 11 ps 1
set style line 12 lc rgb '#9400d3' lt 1 lw 1.5 pt 11 ps 1
set style line 13 lc rgb '#00ffa5' lt 1 lw 1.5 pt 11 ps 1
set style line 14 lc rgb '#800000' lt 1 lw 1.5 pt 11 ps 1
unset key

#Title
#set title "Linearidade de grandezas em fun\\\c{c}\\\~{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\~{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12


#set mxtics 2
#set mytics 10

#Legenda
set key top left vertical title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

max(x)=a+b*x
fit max(x) 'P_Max_log_cos.txt' u 3:5 via a,b

set label 1 '$log(P_{Max})=(0.38\pm 0.05)\cos(\alpha)+(0.69\pm 0.06)$' at 0.55,0.85 rotate by 38 center tc ls 1
######
set xrange [0.1:1.3]
set yrange [0.5:1.3]
set ylabel 'log($P_Max$)' #rotate by  90 center 
plot 'P_Max_log_cos.txt' u 3:5:4:6 title "     $P=P_{Max}$" w xyerrorbars ls 2,\
      max(x) notitle ls 1, \
