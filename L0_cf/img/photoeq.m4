% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                       
cct_init                   

l = 0.75                    
Origin: Here             
   source(up_ l,I);llabel(,I_L,)
   line right 0.4*l
   {
     dot
     diode(down_ l);
     dot
   }
   line right 0.4*l
   {
     dot
     resistor(down_ l);llabel(,R_{SH},)
     dot
   }
   resistor(right_ l);llabel(,R_{S},)
   gap(down_ l);llabel(+,V,-)
   line to Origin
.PE
