reset

#o ficheiro AAP10_2 é importante, NÂO eleminar codigo, comentar se necessário

#epslatex
set terminal epslatex size 24cm,6cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"

#set terminal x11

# color definitions
set border linewidth 1
set style line 1 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 0.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 0.5
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 0.5
set style line 4 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 0.5
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 0.5
set style line 6 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 0.5
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#Legenda
set key top right title 'Graficos P(V)' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set ylabel '$\Delta P/hPa$' offset 2,0
set xlabel '$\Delta d/cm$'  offset 0,0.5

set tics scale 2

set bars 2 #espessura das barras de erro

###############################################################################
#tiks em falta 
#sim podia ter usado uma macro ou codigo mais inteligente e não não me apeteceu 
#ass el garridinho matador de codigo
#set arrow from 6,0    to 11,0 nohead ls 12
#set arrow from 6,200  to 11,200 nohead ls 12
#set arrow from 6,400  to 11,400 nohead ls 12
#set arrow from 6,600  to 11,600 nohead ls 12
#set arrow from 6,800  to 11,800 nohead ls 12
#set arrow from 6,1000 to 11,1000 nohead ls 12
#set arrow from 6,1200 to 11,1200 nohead ls 12
#set arrow from 6,1400 to 11,1400 nohead ls 12

set arrow from 12.4,0 to 17.4,0 nohead ls 12
set arrow from 12.4,200 to 17.4,200 nohead ls 12
set arrow from 12.4,400 to 17.4,400 nohead ls 12
set arrow from 12.4,600 to 17.4,600 nohead ls 12
set arrow from 12.4,800 to 17.4,800 nohead ls 12
set arrow from 12.4,1000 to 17.4,1000 nohead ls 12
set arrow from 12.4,1200 to 17.4,1200 nohead ls 12
set arrow from 12.4,1400 to 17.4,1400 nohead ls 12

set arrow from 18.7,0 to 23.8,0 nohead ls 12
set arrow from 18.7,200 to 23.8,200 nohead ls 12
set arrow from 18.7,400 to 23.8,400 nohead ls 12
set arrow from 18.7,600 to 23.8,600 nohead ls 12
set arrow from 18.7,800 to 23.8,800 nohead ls 12
set arrow from 18.7,1000 to 23.8,1000 nohead ls 12
set arrow from 18.7,1200 to 23.8,1200 nohead ls 12
set arrow from 18.7,1400 to 23.8,1400 nohead ls 12

set arrow from 25,0 to 30,0 nohead ls 12
set arrow from 25,200 to 30,200 nohead ls 12
set arrow from 25,400 to 30,400 nohead ls 12
set arrow from 25,600 to 30,600 nohead ls 12
set arrow from 25,800 to 30,800 nohead ls 12
set arrow from 25,1000 to 30,1000 nohead ls 12
set arrow from 25,1200 to 30,1200 nohead ls 12
set arrow from 25,1400 to 30,1400 nohead ls 12
###############################################################################


#opçao 4 ficheiros separados 
set output 'P_V_AAP10.tex'
plot 't_p_S_AAP10.txt'    u ($3*pi*3*3):2 title "ciclo AAP10"   ls 2

set output 'P_V_AAP10F2.tex'
plot 't_p_S_AAP10F2.txt'  u ($3*pi*3*3):2 title "ciclo AAP10F2" ls 3


set output 'P_V_AAP102.tex'
plot 't_p_S_AAP102.txt'   u ($3*pi*3*3):2 title "ciclo AAP102"  ls 4

set output 'P_V_P10.tex'
plot 't_p_S_P10.txt'      u ($3*pi*3*3):2 title "ciclo P10"     ls 5

###############################################################################
#opçao 1 ficheiro
set output 'P_V.tex'
set multiplot layout 1,4 margins .1,0.95,.2,1 spacing 0.05;#margins left,right,bottom,top
unset key
set xtics 6,1,11
set yrange [-200:1400]
set ytics -200,200,1400
plot 't_p_S_AAP10.txt'    u 3:2 title "ciclo AAP10"   ls 2
unset ylabel
unset ytics
unset arrow
set xtics 6,1,11
plot 't_p_S_AAP10F2.txt'  u 3:2 title "ciclo AAP10F2" ls 3
set xtics 6,1,11
plot 't_p_S_AAP102.txt'   u 3:2 title "ciclo AAP102"  ls 4
set xtics 3,1,11
plot 't_p_S_P10.txt'      u 3:2 title "ciclo P10"     ls 5
unset multiplot
