#      t [ms]     p [hPa]      s [cm]
           0         752        7.92
           1         739        7.94
           2         731        8.04
           3         711        8.07
           4         715        8.11
           5         685        8.18
           6         661        8.22
           7         663        8.31
           8         659        8.37
           9         616        8.40
          10         596        8.44
          11         583        8.48
          12         562        8.57
          13         539        8.63
          14         517        8.66
          15         499        8.73
          16         482        8.79
          17         467        8.84
          18         453        8.89
          19         449        8.98
          20         417        9.03
          21         411        9.08
          22         394        9.14
          23         379        9.23
          24         366        9.31
          25         353        9.35
          26         340        9.39
          27         327        9.43
          28         313        9.50
          29         293        9.57
          30         284        9.64
          31         281        9.69
          32         256        9.73
          33         262        9.79
          34         237        9.83
          35         228        9.90
          36         210        9.96
          37         205       10.02
          38         191       10.05
          39         181       10.10
          40         175       10.17
          41         163       10.19
          42         155       10.25
          43         140       10.31
          44         140       10.35
          45         128       10.42
          46         121       10.45
          47         113       10.48
          48         105       10.51
          49          95       10.56
          50          91       10.60
          51          81       10.62
          52          88       10.67
          53          73       10.71
          54          72       10.75
          55          63       10.78
          56          50       10.82
          57          45       10.84
          58          45       10.86
          59          38       10.89
          60          38       10.90
          61          31       10.93
          62          23       10.93
          63          12       10.93
          64           4       10.96
          65          -1       10.98
          66          -4       10.97
          67          -8       11.01
          68          -9       11.05
          69         -11       11.08
          70         -10       11.08
          71         -12       11.10
          72         -15       11.11
          73         -14       11.08
          74         -20       11.13
          75         -22       11.12
          76         -25       11.12
          77         -29       11.12
          78         -32       11.13
          79         -35       11.12
          80         -35       11.12
          81         -36       11.13
          82         -36       11.10
          83         -35       11.09
          84         -35       11.09
          85         -35       11.10
          86         -35       11.09
          87         -36       11.07
          88         -36       11.06
          89         -37       11.07
          90         -37       11.04
          91         -39       11.04
          92         -39       11.02
          93         -41       10.98
          94         -40       10.98
          95         -39       10.94
          96         -39       10.91
          97         -39       10.91
          98         -38       10.89
          99         -38       10.90
         100         -37       10.89
         101         -35       10.88
         102         -34       10.84
         103         -35       10.82
         104         -33       10.77
         105         -33       10.71
         106         -32       10.68
         107         -31       10.64
         108         -30       10.63
         109         -28       10.57
         110         -29       10.54
         111         -28       10.48
         112         -28       10.48
         113         -24       10.46
         114         -25       10.39
         115         -22       10.34
         116         -20       10.29
         117         -18       10.24
         118         -16       10.19
         119         -11       10.19
         120          -5       10.12
         121          -4       10.06
         122          -2       10.02
         123           4        9.98
         124           7        9.91
         125          12        9.86
         126          18        9.80
         127          26        9.75
         128          35        9.70
         129          40        9.66
         130          47        9.60
         131          56        9.54
         132          63        9.47
         133          66        9.42
         134          77        9.39
         135          83        9.33
         136          92        9.27
         137         102        9.21
         138         112        9.16
         139         126        9.10
         140         134        9.07
         141         136        8.99
         142         150        8.94
         143         161        8.88
         144         166        8.84
         145         177        8.75
         146         188        8.72
         147         193        8.63
         148         202        8.61
         149         217        8.51
         150         226        8.49
         151         235        8.43
         152         248        8.37
         153         268        8.32
         154         283        8.23
         155         294        8.21
         156         303        8.15
         157         309        8.11
         158         344        8.04
         159         367        7.96
         160         383        7.93
         161         396        7.89
         162         413        7.81
         163         428        7.75
         164         443        7.73
         165         464        7.67
         166         482        7.60
         167         499        7.58
         168         520        7.51
         169         536        7.47
         170         542        7.45
         171         563        7.38
         172         574        7.36
         173         591        7.31
         174         610        7.29
         175         632        7.20
         176         645        7.18
         177         665        7.15
         178         682        7.08
         179         696        7.06
         180         712        7.01
         181         726        6.99
         182         742        6.98
         183         753        6.94
         184         762        6.87
         185         771        6.85
         186         770        6.83
         187         783        6.78
         188         805        6.76
         189         827        6.76
         190         855        6.72
         191         861        6.68
         192         869        6.67
         193         902        6.65
         194         914        6.60
         195         913        6.55
         196         953        6.54
         197         952        6.53
         198         975        6.48
         199         983        6.46
         200         997        6.46
         201        1004        6.42
         202        1010        6.41
         203        1037        6.37
         204        1043        6.37
         205        1064        6.36
         206        1076        6.35
         207        1084        6.35
         208        1097        6.32
         209        1101        6.29
         210        1122        6.28
         211        1124        6.27
         212        1136        6.29
         213        1148        6.28
         214        1144        6.25
         215        1164        6.24
         216        1169        6.24
         217        1185        6.21
         218        1183        6.21
         219        1184        6.21
         220        1190        6.21
         221        1200        6.22
         222        1208        6.21
         223        1211        6.21
         224        1223        6.21
         225        1227        6.21
         226        1231        6.23
         227        1237        6.22
         228        1241        6.22
         229        1234        6.23
         230        1240        6.22
         231        1244        6.23
         232        1239        6.23
         233        1246        6.22
         234        1244        6.23
         235        1246        6.26
         236        1247        6.24
         237        1246        6.25
         238        1247        6.26
         239        1248        6.28
         240        1247        6.28
         241        1247        6.29
         242        1242        6.32
         243        1244        6.32
         244        1243        6.35
         245        1241        6.34
         246        1238        6.36
         247        1238        6.39
         248        1234        6.39
         249        1232        6.39
         250        1229        6.43
         251        1222        6.47
         252        1221        6.46
         253        1217        6.48
         254        1215        6.50
         255        1210        6.55
         256        1202        6.56
         257        1196        6.61
         258        1187        6.65
         259        1179        6.68
         260        1174        6.68
         261        1167        6.71
         262        1165        6.75
         263        1161        6.76
         264        1147        6.79
         265        1138        6.85
         266        1118        6.88
         267        1108        6.90
         268        1097        6.95
         269        1076        7.00
         270        1055        7.01
         271        1049        7.05
         272        1030        7.09
         273        1001        7.12
         274        1007        7.16
         275         979        7.23
         276         963        7.27
         277         948        7.32
         278         931        7.37
         279         913        7.42
         280         905        7.45
         281         886        7.46
         282         872        7.54
         283         849        7.59
         284         833        7.64
         285         817        7.70
         286         805        7.72
         287         786        7.78
         288         772        7.86
         289         764        7.91
       
