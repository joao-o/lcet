reset

#um sonho que por falta de dados teve de ser abandonado *tears* #soquenao

#epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"

#set terminal x11

# color definitions
set border linewidth 1
set style line 1 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 1 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 1
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1
set style line 4 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 1
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 1
set style line 6 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

#set xrange [0:9]
#set xtics 1,1,9 #1st,step,last
#set logscale y
set yrange [0.8e2:3.32e2]
set xrange [180:350]
#Legenda
set key top right title 'Graficos P(V)' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set ylabel '$P/kPa$'
set xlabel '$V/cm^3$' #offset 2,0

set tics scale 2

set bars 2 #espessura das barras de erro


#desenho de area de gráfico teórico
set arrow from 197,1.41e2 to 197,3.318e2 nohead ls 6
set arrow from 339,0.8e2 to 339,1.929e2 nohead ls 6

n=0.01128;
tin(x)=(197<x&&x<339)?296.35*n*8.314/x*1e3 :1/0
tout(x)=(197<x&&x<339)?315.25*n*8.314/x*1e3:1/0
tff(x)=(197<x&&x<339)?305.75*n*8.314/x*1e3 :1/0
tfq(x)=(197<x&&x<339)?697.15*n*8.314/x*1e3 :1/0


set samples 1000
set output 'P_V_feio.tex'
plot 't_p_S_AAP102.txt'  u (($3-6.21)*pi*3*3+197):($2*0.1+102.4) title "Dados"  ls 4, \
     tin(x)  title '$H_2O in$'  ls 3, \
     tout(x) title '$H_2O out$' ls 3, \
     tff(x) title 'FF' ls 6, \
     tfq(x) title 'FQ' ls 2
