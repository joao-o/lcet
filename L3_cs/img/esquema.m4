.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   box wid l ht 1.5*l "Motor de" "Stirling"
  
   #circuito
   move to Origin up 0.56*l right 0.1*l
   resistor(right_ 0.8*l);
   line up 0.4*l 
   {
     dot
     source(left_ l*0.8,"V")
     dot
   }
   source(right_ l*0.7,"A")
   line up 0.3*l
   source(left_ l*1.5,"G")
   line down l*0.7
   
   #sensores
   move to Origin up l*0.6
   {line -> left l*.5 "$T_q$" above}; move down 0.4*l
   {line -> left l*.5 "$P$" above}; move down 0.1*l
   {line -> left l*.5 "$s$" below} 

   move to Origin down l*.35
   {line -> left l*.5 "$T_e$" above}
   move down l*0.15
   {line -> left l*.5 "$T_s$" below}
   
   #caixa do computador
   move to Origin left 0.5*l
   box ht 1.5*l wid l*1.1 "Computador"

   #arrefecimento
   move to Origin right l down 0.3*l
   {line right 0.5*l};move down 0.05*l
   {move right 0.1*l; line -> right 0.3*l};move down 0.05*l
   {line right 0.5*l};move down 0.05*l
   {line right 0.5*l};move down 0.05*l
   {move right 0.1*l; line <- right 0.3*l};move down 0.05*l
   line right 0.5*l;move up 0.1*l; right
   box ht 0.7*l wid l "Bomba de" "circulação" "de água"

   move to Origin right l up 0.4*l
   {
     move right 0.12*l up 0.05*l
     spline -> up 0.1*l left 0.05*l then down 0.2*l left 0.05*l then down 0.2*l right 0.1*l
   }
   {L: line right 0.5*l; "Veio" rjust above at L.end };move down 0.1*l
   line right 0.5*l;move up 0.05*l; right
   box ht 0.55*l wid 0.75*l "Travão e" "sensor" "de força"

.PE
