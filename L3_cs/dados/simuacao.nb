(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     22174,        521]
NotebookOptionsPosition[     21423,        491]
NotebookOutlinePosition[     21761,        506]
CellTagsIndexPosition[     21718,        503]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
plot [f(x,y)=Tc+(Th-Tc)*(8/(pi*pi))*(sum[n=0:10] \
(exp(a*(pi/2+n*pi)*(pi/2+n*pi)*y)*(((-1.0)**n)/((2*n+1)**2))*sin((x/0.12)*(pi/\
2+n*pi))))]\
\>", "WolframAlphaLong",
 CellChangeTimes->{{3.636185449524725*^9, 3.636185490074574*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"URLFetch", "::", "invhttp"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"\[NoBreak]\\!\\(\\\"Couldn't resolve host name\\\"\\)\
\[NoBreak]. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/URLFetch\\\", ButtonNote -> \
\\\"URLFetch::invhttp\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.636195292225801*^9}],

Cell[BoxData[
 FrameBox[
  TagBox[GridBox[{
     {
      TagBox[GridBox[{
         {
          DynamicBox[FEPrivate`FrontEndResource["WABitmaps", "WAErrorIcon"],
           ImageSizeCache->{23., {6., 18.}}], Cell[
          "An internet connection could not be established",
           FontWeight->Bold]}
        },
        AutoDelete->False,
        GridBoxItemSize->{
         "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
       "Grid"]},
     {Cell["\<\
This feature requires access to Wolfram|Alpha servers. Please check your \
network connection. You may need to configure your firewall program or set a \
proxy in the Internet Connectivity tab of the Preferences dialog.\
\>"]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
    GridBoxSpacings->{"Columns" -> {{Automatic}}, "Rows" -> {{1.5}}}],
   "Column"],
  Alignment->Top,
  Background->GrayLevel[0.96],
  BaseStyle->{FontFamily -> "Helvetica"},
  FrameMargins->{{20, 20}, {15, 15}},
  FrameStyle->GrayLevel[0.85],
  ImageSize->Automatic,
  RoundingRadius->5,
  StripOnInput->False]], "Print",
 Deployed->True,
 CellChangeTimes->{3.636195292271759*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Tc", "=", "20"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Th", "=", "100"}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{"Animate", "[", 
    RowBox[{
     RowBox[{"Plot3D", "[", 
      RowBox[{
       RowBox[{"Tc", "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"Th", "-", "Tc"}], ")"}], "*", 
         RowBox[{"8", "/", 
          RowBox[{"(", 
           RowBox[{"Pi", "^", "2"}], ")"}]}], "*", 
         RowBox[{"Sum", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Exp", "[", 
             RowBox[{"a", "*", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{
                 RowBox[{"Pi", "/", "2"}], "+", 
                 RowBox[{"n", "*", "Pi"}]}], ")"}], "^", "2"}], "*", "y"}], 
             "]"}], "*", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"-", "1.0"}], ")"}], "^", "n"}], ")"}], "/", 
              RowBox[{"(", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{
                  RowBox[{"2", "*", "n"}], "+", "1"}], ")"}], "^", "2"}], 
               ")"}]}], ")"}], "*", 
            RowBox[{"Sin", "[", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{"x", "/", "12"}], ")"}], "*", 
              RowBox[{"(", 
               RowBox[{
                RowBox[{"Pi", "/", "2"}], "+", 
                RowBox[{"n", "*", "Pi"}]}], ")"}]}], "]"}]}], ",", 
           RowBox[{"{", 
            RowBox[{"n", ",", "0", ",", "10"}], "}"}]}], "]"}]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"x", ",", "0", ",", "15"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"y", ",", "0", ",", "1000"}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"a", ",", 
       RowBox[{"-", "0.01"}], ",", 
       RowBox[{"-", "0.000001"}]}], "}"}]}], "]"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.636185619888315*^9, 3.636185929396619*^9}, {
   3.636186005653368*^9, 3.6361861540569344`*^9}, {3.636186275736184*^9, 
   3.6361864267893867`*^9}, {3.6361864664377813`*^9, 3.636186472241249*^9}, {
   3.636186505539826*^9, 3.6361865251087008`*^9}, {3.6361866010148706`*^9, 
   3.636186609921796*^9}, {3.636186725064499*^9, 3.6361868358730507`*^9}, {
   3.636186898789948*^9, 3.63618690108132*^9}, 3.636186932458439*^9, {
   3.636187643142139*^9, 3.6361876935999203`*^9}, {3.636187754324215*^9, 
   3.636187773901572*^9}, {3.636204150319828*^9, 3.63620419839917*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`a$$ = -0.0022413479185104364`, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`a$$], -0.01, -1.*^-6}}, Typeset`size$$ = {
    33., {6., 9.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`a$171187$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`a$$ = -0.01}, 
      "ControllerVariables" :> {
        Hold[$CellContext`a$$, $CellContext`a$171187$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot3D[$CellContext`Tc + (($CellContext`Th - $CellContext`Tc) (8/Pi^2)) 
         Sum[(Exp[($CellContext`a$$ (
                 Pi/2 + $CellContext`n 
                  Pi)^2) $CellContext`y] ((-1.)^$CellContext`n/(
              2 $CellContext`n + 1)^2)) 
           Sin[($CellContext`x/12) (
              Pi/2 + $CellContext`n Pi)], {$CellContext`n, 0, 
            10}], {$CellContext`x, 0, 15}, {$CellContext`y, 0, 1000}], 
      "Specifications" :> {{$CellContext`a$$, -0.01, -1.*^-6, 
         AppearanceElements -> {
          "ProgressSlider", "PlayPauseButton", "FasterSlowerButtons", 
           "DirectionButton"}}}, 
      "Options" :> {
       ControlType -> Animator, AppearanceElements -> None, DefaultBaseStyle -> 
        "Animate", DefaultLabelStyle -> "AnimateLabel", SynchronousUpdating -> 
        True, ShrinkingDelay -> 10.}, "DefaultOptions" :> {}],
     ImageSizeCache->{407., {175., 182.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Animate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.6361877473611717`*^9, 3.636187789244821*^9}, 
   3.636195295988428*^9, {3.636204133591065*^9, 3.636204200223068*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.636185805681588*^9, 3.6361858094300117`*^9}, {
  3.636185846214217*^9, 3.636185846513275*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"DSolve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"u", "[", 
           RowBox[{"t", ",", "x"}], "]"}], ",", "t"}], "]"}], "==", 
        RowBox[{
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"u", "[", 
            RowBox[{"t", ",", "x"}], "]"}], ",", "x", ",", "x"}], "]"}], "-", 
         
         RowBox[{
          RowBox[{"(", 
           RowBox[{"u", "[", 
            RowBox[{"t", ",", "x"}], "]"}], ")"}], "^", "4"}]}]}], ",", 
       RowBox[{
        RowBox[{"u", "[", 
         RowBox[{"t", ",", "0"}], "]"}], "\[Equal]", 
        RowBox[{"5", "+", 
         RowBox[{"3", "*", "x"}]}]}], ",", 
       RowBox[{
        RowBox[{"u", "[", 
         RowBox[{"t", ",", "5"}], "]"}], "\[Equal]", "3"}]}], "}"}], ",", 
     RowBox[{"u", "[", 
      RowBox[{"t", ",", "x"}], "]"}], ",", "t", ",", "x"}], "]"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.636186072373801*^9, 3.636186074165123*^9}, 
   3.636186370954928*^9, {3.636209056055794*^9, 3.636209064230905*^9}, {
   3.636209173265027*^9, 3.636209181954527*^9}, {3.636209237856749*^9, 
   3.636209241980968*^9}, {3.636209276020052*^9, 3.63620928740993*^9}, {
   3.6362093229615307`*^9, 3.636209442859576*^9}, {3.636209487146201*^9, 
   3.636209491235415*^9}, {3.636209521479129*^9, 3.6362095249468718`*^9}, {
   3.636209622866371*^9, 3.636209673284416*^9}, {3.636209735806638*^9, 
   3.636209740300425*^9}}],

Cell[BoxData[
 RowBox[{"DSolve", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{
       SuperscriptBox["u", 
        TagBox[
         RowBox[{"(", 
          RowBox[{"1", ",", "0"}], ")"}],
         Derivative],
        MultilineFunction->None], "[", 
       RowBox[{"t", ",", "x"}], "]"}], "\[Equal]", 
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox[
         RowBox[{"u", "[", 
          RowBox[{"t", ",", "x"}], "]"}], "4"]}], "+", 
       RowBox[{
        SuperscriptBox["u", 
         TagBox[
          RowBox[{"(", 
           RowBox[{"0", ",", "2"}], ")"}],
          Derivative],
         MultilineFunction->None], "[", 
        RowBox[{"t", ",", "x"}], "]"}]}]}], ",", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"t", ",", "0"}], "]"}], "\[Equal]", 
      RowBox[{"5", "+", 
       RowBox[{"3", " ", "x"}]}]}], ",", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"t", ",", "5"}], "]"}], "\[Equal]", "3"}]}], "}"}], ",", 
   RowBox[{"u", "[", 
    RowBox[{"t", ",", "x"}], "]"}], ",", "t", ",", "x"}], "]"}]], "Output",
 CellChangeTimes->{{3.6362095076955147`*^9, 3.636209526802249*^9}, {
   3.636209624994526*^9, 3.636209662824418*^9}, 3.636209744227621*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
53.01*x^(-0.118)==34.28-0.00844*x
\
\>", "WolframAlphaLong",
 CellChangeTimes->{{3.636216774560625*^9, 3.636216786699984*^9}, {
  3.636217017705649*^9, 3.636217051700623*^9}, {3.636226307848175*^9, 
  3.636226398888619*^9}}],

Cell[BoxData[
 NamespaceBox["WolframAlphaQueryResults",
  DynamicModuleBox[{Typeset`q$$ = "53.01*x^(-0.118)==34.28-0.00844*x", 
   Typeset`opts$$ = {
   AppearanceElements -> {
     "Warnings", "Assumptions", "Brand", "Pods", "PodMenus", "Unsuccessful", 
      "Sources"}, Asynchronous -> All, 
    TimeConstraint -> {30, Automatic, Automatic, Automatic}, 
    Method -> {
     "Formats" -> {"cell", "minput", "msound", "dataformats"}, "Server" -> 
      "http://api.wolframalpha.com/v1/"}}, Typeset`elements$$ = {
   "Warnings", "Assumptions", "Brand", "Pods", "PodMenus", "Unsuccessful", 
    "Sources"}, Typeset`pod1$$ = XMLElement[
   "pod", {"title" -> "Input", "scanner" -> "Identity", "id" -> "Input", 
     "position" -> "100", "error" -> "false", "numsubpods" -> "1"}, {
     XMLElement["subpod", {"title" -> ""}, {
       XMLElement["minput", {}, {"53.01/x^0.118 == 34.28 - 0.00844 x"}], 
       XMLElement["cell", {"compressed" -> False, "string" -> True}, {
         Cell[
          BoxData[
           FormBox[
            TagBox[
             RowBox[{
               FractionBox["53.01`", 
                SuperscriptBox["x", "0.118`"]], "\[LongEqual]", 
               TagBox[
                RowBox[{
                  RowBox[{"34.28`", "\[VeryThinSpace]"}], "-", 
                  RowBox[{"0.00844`", " ", "x"}]}], Identity]}], 
             PolynomialForm[#, TraditionalOrder -> False]& ], 
            TraditionalForm]], "Output", {}]}], 
       XMLElement[
       "dataformats", {}, {
        "plaintext,minput,computabledata,formatteddata,formuladata"}]}]}], 
   Typeset`pod2$$ = XMLElement[
   "pod", {"title" -> "Numerical solution", "scanner" -> "Reduce", "id" -> 
     "NumericalSolution", "position" -> "200", "error" -> "false", 
     "numsubpods" -> "1"}, {
     XMLElement["subpod", {"title" -> ""}, {
       XMLElement[
       "minput", {}, {
        "FindRoot[-34.28 + 53.01/x^0.118 + 0.00844 x == 0, {x, 41.5373, \
45.5827}, WorkingPrecision -> 15]"}], 
       XMLElement["cell", {"compressed" -> False, "string" -> True}, {
         Cell[
          BoxData[
           FormBox[
            
            TemplateBox[{
             "x", "\" \[TildeTilde] \"", 
              "44.11374962711625142733841028839285228287`15.", 
              "\"\[Ellipsis]\""}, "RowDefault"], TraditionalForm]], 
          "Output", {}]}], 
       XMLElement[
       "dataformats", {}, {
        "plaintext,minput,moutput,computabledata,formatteddata"}]}], 
     XMLElement["states", {"count" -> "1"}, {
       XMLElement[
       "state", {
        "name" -> "More digits", "input" -> 
         "NumericalSolution__More digits"}, {}]}]}], Typeset`pod3$$, 
   Typeset`pod4$$, Typeset`pod5$$, Typeset`pod6$$, Typeset`pod7$$, 
   Typeset`pod8$$, Typeset`pod9$$, Typeset`pod10$$, Typeset`pod11$$, 
   Typeset`pod12$$, Typeset`pod13$$, Typeset`pod14$$, Typeset`pod15$$, 
   Typeset`pod16$$, Typeset`pod17$$, Typeset`pod18$$, Typeset`pod19$$, 
   Typeset`pod20$$, Typeset`pod21$$, Typeset`pod22$$, Typeset`pod23$$, 
   Typeset`pod24$$, Typeset`pod25$$, Typeset`pod26$$, Typeset`pod27$$, 
   Typeset`pod28$$, Typeset`pod29$$, Typeset`pod30$$, Typeset`pod31$$, 
   Typeset`pod32$$, Typeset`aux1$$ = {True, False, {False}, True}, 
   Typeset`aux2$$ = {True, False, {False}, True}, Typeset`aux3$$ = {
   True, False, {False}, True}, Typeset`aux4$$ = {True, False, {False}, True},
    Typeset`aux5$$ = {True, False, {False}, True}, Typeset`aux6$$ = {
   True, False, {False}, True}, Typeset`aux7$$ = {True, False, {False}, True},
    Typeset`aux8$$ = {True, False, {False}, True}, Typeset`aux9$$ = {
   True, False, {False}, True}, Typeset`aux10$$ = {
   True, False, {False}, True}, Typeset`aux11$$ = {
   True, False, {False}, True}, Typeset`aux12$$ = {
   True, False, {False}, True}, Typeset`aux13$$ = {
   True, False, {False}, True}, Typeset`aux14$$ = {
   True, False, {False}, True}, Typeset`aux15$$ = {
   True, False, {False}, True}, Typeset`aux16$$ = {
   True, False, {False}, True}, Typeset`aux17$$ = {
   True, False, {False}, True}, Typeset`aux18$$ = {
   True, False, {False}, True}, Typeset`aux19$$ = {
   True, False, {False}, True}, Typeset`aux20$$ = {
   True, False, {False}, True}, Typeset`aux21$$ = {
   True, False, {False}, True}, Typeset`aux22$$ = {
   True, False, {False}, True}, Typeset`aux23$$ = {
   True, False, {False}, True}, Typeset`aux24$$ = {
   True, False, {False}, True}, Typeset`aux25$$ = {
   True, False, {False}, True}, Typeset`aux26$$ = {
   True, False, {False}, True}, Typeset`aux27$$ = {
   True, False, {False}, True}, Typeset`aux28$$ = {
   True, False, {False}, True}, Typeset`aux29$$ = {
   True, False, {False}, True}, Typeset`aux30$$ = {
   True, False, {False}, True}, Typeset`aux31$$ = {
   True, False, {False}, True}, Typeset`aux32$$ = {
   True, False, {False}, True}, Typeset`asyncpods$$ = {}, 
   Typeset`nonpods$$ = {}, Typeset`initdone$$ = True, 
   Typeset`queryinfo$$ = {{
    "success" -> "true", "error" -> "false", "numpods" -> "2", "datatypes" -> 
     "", "timedout" -> 
     "Reduce,Derivative,Plotter,ComplexMap,GlobalExtrema,Inequality2D,\
Geometry,Inequality,Simplification,ImplicitDifferentiation,\
FredholmIntegralEquation", "timedoutpods" -> "", "timing" -> "3.924", 
     "parsetiming" -> "0.426", "parsetimedout" -> "false", "recalculate" -> 
     "http://www5a.wolframalpha.com/api/v2/recalc.jsp?id=\
MSPa501220dba4cfcc2a6ec500003c262h97c9832fdg&s=40", "id" -> 
     "MSPa501320dba4cfcc2a6ec5000041cib6a9a3b080g6", "host" -> 
     "http://www5a.wolframalpha.com", "server" -> "40", "related" -> 
     "http://www5a.wolframalpha.com/api/v2/relatedQueries.jsp?id=\
MSPa501420dba4cfcc2a6ec5000058701323g796d7bg&s=40", "version" -> "2.6"}, {
    "success" -> "true", "error" -> "false", "numpods" -> "0", "datatypes" -> 
     "", "timedout" -> 
     "Reduce,Derivative,Plotter,ComplexMap,GlobalExtrema,Inequality2D,\
Geometry,Inequality,Simplification,ImplicitDifferentiation,\
FredholmIntegralEquation", "timedoutpods" -> "", "timing" -> "5.064", 
     "parsetiming" -> "0.", "parsetimedout" -> "false", "recalculate" -> "", 
     "id" -> "", "host" -> "http://www5a.wolframalpha.com", "server" -> "40", 
     "related" -> "", "version" -> "2.6"}}, Typeset`sessioninfo$$ = {
   "TimeZone" -> 0., 
    "Date" -> {2015, 3, 24, 22, 53, 28.894886`8.213395966891508}, "Line" -> 
    119, "SessionID" -> 23059505222320953673}, Typeset`showpods$$ = {1, 2}, 
   Typeset`failedpods$$ = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 
   17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}, 
   Typeset`chosen$$ = {}, Typeset`open$$ = False, Typeset`newq$$ = 
   "53.01*x^(-0.118)==34.28-0.00844*x"}, 
   DynamicBox[ToBoxes[
     AlphaIntegration`FormatAlphaResults[
      Dynamic[{
       2, {Typeset`pod1$$, Typeset`pod2$$, Typeset`pod3$$, Typeset`pod4$$, 
         Typeset`pod5$$, Typeset`pod6$$, Typeset`pod7$$, Typeset`pod8$$, 
         Typeset`pod9$$, Typeset`pod10$$, Typeset`pod11$$, Typeset`pod12$$, 
         Typeset`pod13$$, Typeset`pod14$$, Typeset`pod15$$, Typeset`pod16$$, 
         Typeset`pod17$$, Typeset`pod18$$, Typeset`pod19$$, Typeset`pod20$$, 
         Typeset`pod21$$, Typeset`pod22$$, Typeset`pod23$$, Typeset`pod24$$, 
         Typeset`pod25$$, Typeset`pod26$$, Typeset`pod27$$, Typeset`pod28$$, 
         Typeset`pod29$$, Typeset`pod30$$, Typeset`pod31$$, 
         Typeset`pod32$$}, {
        Typeset`aux1$$, Typeset`aux2$$, Typeset`aux3$$, Typeset`aux4$$, 
         Typeset`aux5$$, Typeset`aux6$$, Typeset`aux7$$, Typeset`aux8$$, 
         Typeset`aux9$$, Typeset`aux10$$, Typeset`aux11$$, Typeset`aux12$$, 
         Typeset`aux13$$, Typeset`aux14$$, Typeset`aux15$$, Typeset`aux16$$, 
         Typeset`aux17$$, Typeset`aux18$$, Typeset`aux19$$, Typeset`aux20$$, 
         Typeset`aux21$$, Typeset`aux22$$, Typeset`aux23$$, Typeset`aux24$$, 
         Typeset`aux25$$, Typeset`aux26$$, Typeset`aux27$$, Typeset`aux28$$, 
         Typeset`aux29$$, Typeset`aux30$$, Typeset`aux31$$, Typeset`aux32$$}, 
        Typeset`chosen$$, Typeset`open$$, Typeset`elements$$, Typeset`q$$, 
        Typeset`opts$$, Typeset`nonpods$$, Typeset`queryinfo$$, 
        Typeset`sessioninfo$$, Typeset`showpods$$, Typeset`failedpods$$, 
        Typeset`newq$$}]], StandardForm],
    ImageSizeCache->{880., {53., 59.}},
    TrackedSymbols:>{Typeset`showpods$$, Typeset`failedpods$$}],
   DynamicModuleValues:>{},
   Initialization:>If[
     Not[Typeset`initdone$$], Null; AlphaIntegration`DoAsyncInitialization[
       Hold[{
        2, {Typeset`pod1$$, Typeset`pod2$$, Typeset`pod3$$, Typeset`pod4$$, 
          Typeset`pod5$$, Typeset`pod6$$, Typeset`pod7$$, Typeset`pod8$$, 
          Typeset`pod9$$, Typeset`pod10$$, Typeset`pod11$$, Typeset`pod12$$, 
          Typeset`pod13$$, Typeset`pod14$$, Typeset`pod15$$, Typeset`pod16$$, 
          Typeset`pod17$$, Typeset`pod18$$, Typeset`pod19$$, Typeset`pod20$$, 
          Typeset`pod21$$, Typeset`pod22$$, Typeset`pod23$$, Typeset`pod24$$, 
          Typeset`pod25$$, Typeset`pod26$$, Typeset`pod27$$, Typeset`pod28$$, 
          Typeset`pod29$$, Typeset`pod30$$, Typeset`pod31$$, 
          Typeset`pod32$$}, {
         Typeset`aux1$$, Typeset`aux2$$, Typeset`aux3$$, Typeset`aux4$$, 
          Typeset`aux5$$, Typeset`aux6$$, Typeset`aux7$$, Typeset`aux8$$, 
          Typeset`aux9$$, Typeset`aux10$$, Typeset`aux11$$, Typeset`aux12$$, 
          Typeset`aux13$$, Typeset`aux14$$, Typeset`aux15$$, Typeset`aux16$$, 
          Typeset`aux17$$, Typeset`aux18$$, Typeset`aux19$$, Typeset`aux20$$, 
          Typeset`aux21$$, Typeset`aux22$$, Typeset`aux23$$, Typeset`aux24$$, 
          Typeset`aux25$$, Typeset`aux26$$, Typeset`aux27$$, Typeset`aux28$$, 
          Typeset`aux29$$, Typeset`aux30$$, Typeset`aux31$$, Typeset`aux32$$},
          Typeset`chosen$$, Typeset`open$$, Typeset`elements$$, Typeset`q$$, 
         Typeset`opts$$, Typeset`nonpods$$, Typeset`queryinfo$$, 
         Typeset`sessioninfo$$, Typeset`showpods$$, Typeset`failedpods$$, 
         Typeset`newq$$, 
         "http://www5a.wolframalpha.com/api/v2/recalc.jsp?id=\
MSPa501220dba4cfcc2a6ec500003c262h97c9832fdg&s=40", Typeset`asyncpods$$}]]; 
     Typeset`asyncpods$$ = {}; Typeset`initdone$$ = True],
   SynchronousInitialization->False],
  BaseStyle->{Deployed -> True},
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True]], "Print",
 CellMargins->{{20, 10}, {Inherited, Inherited}},
 CellChangeTimes->{3.636226409048513*^9}]
}, Open  ]]
},
WindowSize->{959, 1020},
WindowMargins->{{70, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (December 4, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 244, 5, 44, "WolframAlphaLong"],
Cell[827, 29, 442, 10, 23, "Message"],
Cell[1272, 41, 1238, 35, 122, "Print"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2547, 81, 2655, 68, 231, "Input"],
Cell[5205, 151, 2431, 47, 374, "Output"]
}, Open  ]],
Cell[7651, 201, 143, 2, 32, "Input"],
Cell[CellGroupData[{
Cell[7819, 207, 1619, 41, 143, "Input"],
Cell[9441, 250, 1226, 38, 38, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10704, 293, 234, 6, 52, "WolframAlphaLong"],
Cell[10941, 301, 10466, 187, 122, "Print"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
