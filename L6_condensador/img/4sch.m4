% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   battery(up_ l); llabel(,"$V_{eq}$",);
   resistor(right_ l); llabel(,"$R_{eq}$",);
   {
     dot
     capacitor(down_ l); rlabel(,"$C$",);
     dot
   }
   line right 0.4*l
   source(down l,"V");
   line to Origin
   ground(,)
.PE
