% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 
cct_init                       # Read in macro definitions and set defaults
Origin: Here                   # Position names are capitalized
battery(up_ 0.8);llabel(,"$9V$",)
line right 0.3
{
  dot
  source(down_ 0.8,"V",0.2);
  dot
}
line right 0.2
dot
move right 0.3
dot
resistor(right_ 0.5);llabel(,"$R$",);
{
  dot
  capacitor(down_ 0.8);llabel(,"$C$",);
  dot
}
line right 0.3
gap(down_ 0.8);larrow("$u_c$");
line left 0.95
{
  dot
  line up 0.65
  dot
}
line  to Origin
dot
ground
.PE
