% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,S); llabel(,"$U_1$",);
   resistor(right_ l); llabel(,"$R$",);
   {
     resistor(down_ l); rlabel(,"$R_{s}$",);
   }
   line right 0.5*l
   {
     resistor(down_ l); rlabel(,"$R_{c}$",);
   }
   line right 0.25*l
   {
   gap(down_ l); llabel("$T$",,"$T$");
   }
   line right 0.25*l
   {
     capacitor(down_ l); rlabel(,"$C_{s}$",);
   }
   line right 0.5*l
   {
     capacitor(down_ l); rlabel(,"$C_{c}$",);
   }

   line right 0.4*l
   gap(down_ l); llabel(+,"$U_2$",-);
   line to Origin
   ground(,)
.PE
