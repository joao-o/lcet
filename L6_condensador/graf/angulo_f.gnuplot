reset

# epslatex
set terminal epslatex size 21cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'angulo_f.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#000f0f' lt 1 lw 1.5 pt 6 ps 2

set key top left

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 15 back ls 11
set tics nomirror out scale 0.2
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics y2tics x2tics ls 12
set x2tics 2e-13
set x2range[8.6e-12:9.7e-12]
set format x2 '$%.1s\times 10^{%S}$';
set y2tics 2e-12
set y2range[-2e-12:1e-11]
set format y2 '$%.1s\times 10^{%S}$';
set yrange [0:1.2]

#axis labels
set xlabel '$f(Hz)$'
set ylabel '$angulo$'
set x2label '$Re(\varepsilon)$' offset -1
set y2label '$Im(\varepsilon)$' offset -1

set tics scale 2

set logscale x 10


plot 'angulo_f.txt' u 1:2:3 axes x1y1 title "angulo(f)" w yerrorbars ls 3,\
     'ReeIme.txt' u 1:3:2:4 axes x2y2 title '$Im(\varepsilon)(Re(\varepsilon))$' w xyerrorbars ls 4

