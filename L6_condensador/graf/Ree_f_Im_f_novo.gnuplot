reset

# epslatex
set terminal epslatex size 24cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Ree_f_Im_f_novo.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2
set style line 2 lc rgb '#000000' lt 1 lw 2   pt 6 ps 1.5
set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#000f0f' lt 1 lw 1.5 pt 1 ps 3

set key top title 'Legenda' nobox

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.2
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics y2tics ls 12
set format y '$%.2s\times 10^{%S}$';
set format y2 '$%.2s\times 10^{%S}$';
set xrange[900:500000]
set ytics 2e-13
set yrange[9e-12:10.1e-12]
set y2tics 1e-13
#set y2range[0:10e-12]

#axis labels
set xlabel  '$f(Hz)$'
set ylabel  sprintf('\shortstack{$~Re(\varepsilon)$}') offset 3.1
set y2label '$Im(\varepsilon)$' offset -1.1

set tics scale 2

set logscale x 10

b=10
lin(x)=b;
fit lin(x) 'todos_os_dados.txt' every ::1::5  using 1:6:7 yerror via b
plot 'todos_os_dados.txt' u 1:6:7 axes x1y1 title '$Re(\varepsilon)$' w yerrorbars ls 3,\
     'todos_os_dados.txt' u 1:8:9 axes x1y2 title '$Im(\varepsilon)$' w yerrorbars ls 2,\
      lin(x) axes x1y1 title sprintf('$Re(\varepsilon)=%0.4g\pm %0.1g$',b,b_err) ls 2     
