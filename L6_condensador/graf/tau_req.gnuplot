reset

# epslatex
set terminal epslatex size 16cm,10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'tau_req.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#000088' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#880000' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#ranges
#set xrange [-2:10]
#set yrange [0:1.2]

#Legenda
set key top left title 'Legenda' nobox

#axis labels
set xlabel '$R(k \Omega)$'
set ylabel '$1/ \tau (ms^{-1})$' #rotate by  90 center 


set bars 2

#fit linear
lin(x)=b+a*x
b=0.001;
a=1;
fit lin(x) 'tau_r_D.txt' using (($1*545)/($1+545)):2:(($1*545)/(($1+545)*($1+545))*(1/$1+10/545)):3 xyerror via a,b 
l(x)=d+c*x
d=0.001;
c=1;
fit l(x) 'tau_r_C.txt' using (($1*545)/($1+545)):2:(($1*545)/(($1+545)*($1+545))*(1/$1+10/545)):3 xyerror via c,d 
t(x)=1*x;

plot 'tau_r_D.txt' u (($1*545)/($1+545)):2:(($1*545)/(($1+545)*($1+545))*(1/$1+10/545)):3 title 'Descarga' w xyerrorbars ls 1, \
       lin(x) title sprintf('$1/\tau=(%1.3f\pm %0.3f) R+%1.2f\pm %0.2f$',a,a_err,b,b_err) ls 2, \
      'tau_r_C.txt' u (($1*545)/($1+545)):2:(($1*545)/(($1+545)*($1+545))*(1/$1+10/545)):3 title 'Carga' w xyerrorbars ls 1, \
       l(x) title sprintf('$1/\tau=(%1.3f\pm %0.3f) R+%1.2f\pm %0.2f$',c,c_err,d,d_err) ls 3, \
	t(x) title 'Declive teórico' ls 1

