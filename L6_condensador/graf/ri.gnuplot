reset

# epslatex
set terminal epslatex size 16cm,10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'ri.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#898989' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#0000FF' lt 1 lw 1.5 pt 6 ps 1.5
set style line 4 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#ranges
#set xrange [-2:10]
set yrange [6.7:7.9]

#Legenda
set key top right title 'Legenda' nobox

#axis labels
set xlabel '$R(k \Omega)$'
set ylabel '$V$' #rotate by  90 center 

set bars 2

#fit linear
l(x)=(7.875*b)/(b+x)
b=500;
fit l(x) 'ri_C.txt' using 1:2:(1):(0.005) xyerror via b

plot 'ri_C.txt' u 1:2:(1):(0.005) title 'Carga' w xyerrorbars ls 1, \
     'ri_D.txt' u 1:2:(1):(0.005) title 'Descarga' w xyerrorbars ls 2, \
     l(x) title sprintf('$R_i=(%1f\pm %1f)k\Omega$',b,b_err) ls 4,


