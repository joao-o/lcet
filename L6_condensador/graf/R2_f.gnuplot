reset

# epslatex
set terminal epslatex size 18cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'R2_f.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#000f0f' lt 1 lw 1.5 pt 1 ps 3

unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.2
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics ls 12

#set xrange [10:100000]
#set yrange [-40:5]

#axis labels
set xlabel '$f(Hz)$'
set ylabel '$R_2(\omega)$'

set tics scale 2

set logscale x 10


plot 'R2_f.txt' u 1:2:3 title "Dados" w yerrorbars ls 3
