reset

# epslatex
set terminal epslatex size 18cm,12cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'C1UR10KC.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 2
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 2
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 2
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 2
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 2
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#ranges
set xrange [0:0.3]
set yrange [-2.05:2.3]

#Legenda
set key top right title 'Legenda' nobox 

#axis labels
set xlabel '$t/s$'
set ylabel '$\log(V)$' #rotate by  90 center 


set bars 2

#fit linear
lindez(x)=g*x+b
g=-0.4;b=0.8;
fit lindez(x) "<(sed -n '2,255p' C1UR10KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via g,b
linvinte(x)=a*x+c
a=-0.4;c=0.8;
fit linvinte(x) "<(sed -n '2,255p' C1UR20KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via a,c
lintrinta(x)=d*x+e
d=-0.4;e=0.8;
fit lintrinta(x) "<(sed -n '2,255p' C1UR30KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via d,e
linquarenta(x)=f*x+h
f=-0.4;h=0.8;
fit linquarenta(x) "<(sed -n '2,255p' C1UR40KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via f,h
lincinquenta(x)=i*x+j
i=-0.4;j=0.8;
fit lincinquenta(x) "<(sed -n '2,255p' C1UR50KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via i,j
linsessenta(x)=k*x+l
k=-0.4;l=0.8;
fit linsessenta(x) "<(sed -n '2,255p' C1UR60KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via k,l
linsetenta(x)=m*x+o
m=-0.4;o=0.8;
fit linsetenta(x) "<(sed -n '2,255p' C1UR70KC.txt)" \
  using ($1/1000):(log($3)):(0.1/1000):(0.01/$2) xyerror via m,o

plot 'C1UR10KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) title 'Dados' w xyerrorbars ls 1, \
     'C1UR20KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     'C1UR30KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     'C1UR40KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     'C1UR50KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     'C1UR60KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     'C1UR70KC.txt' every 15::1 \
       u (($1)/1000):(log($3)):(0.1/1000):(0.01/$1) notitle w xyerrorbars ls 1, \
     lindez(x) title sprintf('$\log(V)=(%1.2f\pm %0.2f)t+(%1.3f\pm %0.3f)~R=10k\Omega$'\
       ,g,g_err,b,b_err) ls 2, \
     linvinte(x) title sprintf('$\log(V)=(%1.2f\pm %0.2f)t+(%1.4f\pm %0.4f)~R=20k\Omega$'\
       ,a,a_err,c,c_err) ls 3, \
     lintrinta(x) title sprintf('$\log(V)=(%1.3f\pm %0.3f)t+(%1.3f\pm %0.3f)~R=30k\Omega$'\
       ,d,d_err,e,e_err) ls 4, \
     linquarenta(x) title sprintf('$\log(V)=(%1.3f\pm %0.3f)t+(%1.3f\pm %0.3f)~R=40k\Omega$'\
       ,f,f_err,h,h_err) ls 5, \
     lincinquenta(x) title sprintf('$\log(V)=(%1.3f\pm %0.3f)t+(%1.4f\pm %0.4f)~R=50k\Omega$'\
       ,i,i_err,j,j_err) ls 6, \
     linsessenta(x) title sprintf('$\log(V)=(%1.3f\pm %0.3f)t+(%1.4f\pm %0.4f)~R=60k\Omega$'\
       ,k,k_err,l,l_err) ls 7, \
     linsetenta(x) title sprintf('$\log(V)=(%1.3f\pm %0.3f)t+(%1.4f\pm %0.4f)~R=70k\Omega$'\
       ,m,m_err,o,o_err) ls 8
