reset

# epslatex
set terminal epslatex size 23cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'R2_f_C_f_novo.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2
set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 5 lc rgb '#FF4035' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#000f0f' lt 1 lw 1.5 pt 1 ps 3

set key bottom left title 'Legenda' nobox

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.2
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics y2tics ls 12
#set format y '$%.2s\times 10^{%S}$';
#set ytics 2e-13
#set yrange[0:1.4e6]
set xrange[900:130000]
set y2tics 10e-11
set y2range[4.8e-9:5.3e-9]
set format y2 '$%.1s\times 10^{%S}$';


#axis labels
set xlabel '$f(Hz)$'
set ylabel '$R_{eq}(\Omega)$'
set y2label '$C_{eq}(F)$' offset -2.5
set tics scale 2

set logscale x 10

b=5e-10;
lin(x)=b;
fit lin(x) 'todos_os_dados.txt' every ::1::5  using 1:4:5 yerror via b


plot 'todos_os_dados.txt' u 1:2:3 axes x1y1 title '$R_{eq}(f)$' w yerrorbars ls 3,\
     'todos_os_dados.txt' u 1:4:5 axes x1y2 title '$C(f)$' w yerrorbars ls 5,\
      lin(x) axes x1y2 title sprintf('$C(F)=%0.4g\pm %0.2g$',b,b_err) ls 2
