.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   resistor(up_ l);rlabel(,R,)
   line right l*0.5
   source(right_ l,"A");
   dot
   {
    source(down_ Here.y-Origin.y,"V");
    dot
   }
   line right_ l*0.5
   move down l
   {battery(up_ l); variable()}
   line to Origin
.PE
