(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19627,        470]
NotebookOptionsPosition[     18529,        427]
NotebookOutlinePosition[     18867,        442]
CellTagsIndexPosition[     18824,        439]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"T_", ",", "l_", ",", "n_"}], "]"}], ":=", 
   RowBox[{"2", "Pi", "*", 
    RowBox[{"(", 
     RowBox[{"6.626", "*", 
      RowBox[{"10", "^", 
       RowBox[{"(", 
        RowBox[{"-", "34"}], ")"}]}]}], ")"}], "*", 
    RowBox[{
     RowBox[{
      RowBox[{"(", "299792458", ")"}], "^", "2"}], "/", 
     RowBox[{"(", 
      RowBox[{"l", "^", "n"}], ")"}]}], "*", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Exp", "[", 
        RowBox[{"6.626", "*", 
         RowBox[{"10", "^", 
          RowBox[{"(", 
           RowBox[{"-", "34"}], ")"}]}], "*", 
         RowBox[{"299792458", "/", 
          RowBox[{"(", 
           RowBox[{"1.380658", "*", 
            RowBox[{"10", "^", 
             RowBox[{"(", 
              RowBox[{"-", "23"}], ")"}]}], "*", "T", "*", "l"}], ")"}]}]}], 
        "]"}], "-", "1"}], ")"}], "^", 
     RowBox[{"(", 
      RowBox[{"-", "1"}], ")"}]}]}]}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.63561833969923*^9, 3.6356186670723343`*^9}, 
   3.635618729501204*^9, {3.63561887374409*^9, 3.6356188740405283`*^9}, {
   3.63561904613171*^9, 3.6356191169068737`*^9}, {3.635619181265764*^9, 
   3.635619209658476*^9}, {3.635619479217801*^9, 3.635619495948059*^9}, {
   3.635620355825692*^9, 3.635620363004311*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"a", ",", "b", ",", "4"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"b", ",", 
     RowBox[{"360", "*", 
      RowBox[{"10", "^", 
       RowBox[{"(", 
        RowBox[{"-", "9"}], ")"}]}]}], ",", 
     RowBox[{"750", "*", 
      RowBox[{"10", "^", 
       RowBox[{"(", 
        RowBox[{"-", "9"}], ")"}]}]}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.635619302994643*^9, 3.635619303691375*^9}, {
  3.6356196747161427`*^9, 3.6356197842491207`*^9}}],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["\[Integral]", 
   FractionBox["9", "25000000"], 
   FractionBox["3", "4000000"]], 
  RowBox[{
   FractionBox["3.741732238169742`*^-16", 
    RowBox[{
     SuperscriptBox["b", "4"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox["0.014387522664613543`", 
         RowBox[{"a", " ", "b"}]]]}], ")"}]}]], 
   RowBox[{"\[DifferentialD]", "b"}]}]}]], "Output",
 CellChangeTimes->{
  3.6356197489346113`*^9, 3.635619785853166*^9, 3.63561983418165*^9, 
   3.635619949960557*^9, 3.635619999475113*^9, 3.635620097995788*^9, 
   3.635620302828588*^9, 3.6356203691073847`*^9, {3.635620462181451*^9, 
   3.6356205029392433`*^9}, {3.635620631877554*^9, 3.635620661480713*^9}, {
   3.6356208493269777`*^9, 3.635620871838423*^9}, 3.635621416956861*^9, 
   3.635621647143675*^9, 3.6356221870490637`*^9, 3.635622237876601*^9, {
   3.635622320962111*^9, 3.635622333328376*^9}, 3.635622611805646*^9, 
   3.635624651260778*^9, 3.635624964981826*^9, 3.635625090267157*^9, {
   3.6356252138619127`*^9, 3.635625234322925*^9}, 3.635625288730764*^9, 
   3.635625325388638*^9, 3.635709531438682*^9, 3.635709626573295*^9, {
   3.6357097389732113`*^9, 3.635709750867778*^9}, {3.6357097810925217`*^9, 
   3.635709792224956*^9}, 3.635709844915051*^9, {3.635709878801901*^9, 
   3.635709895997467*^9}, {3.635709934939125*^9, 3.6357099419250107`*^9}, {
   3.635710008142761*^9, 3.6357100195703897`*^9}, 3.635710107183505*^9, {
   3.6357101385309467`*^9, 3.635710160478023*^9}, 3.635710196034465*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"a", ",", "d", ",", "4"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"d", ",", "0", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.635619795077313*^9, 3.635619830410095*^9}}],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["\[Integral]", "0", "10"], 
  RowBox[{
   FractionBox["3.741732238169742`*^-16", 
    RowBox[{
     SuperscriptBox["d", "4"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox["0.014387522664613543`", 
         RowBox[{"a", " ", "d"}]]]}], ")"}]}]], 
   RowBox[{"\[DifferentialD]", "d"}]}]}]], "Output",
 CellChangeTimes->{{3.6356198169901953`*^9, 3.635619834861178*^9}, 
   3.6356199506474543`*^9, 3.635620000109234*^9, 3.635620098623901*^9, 
   3.635620303445283*^9, 3.635620369725233*^9, {3.6356204628510523`*^9, 
   3.635620503571486*^9}, {3.635620632820511*^9, 3.63562066211565*^9}, {
   3.635620850278509*^9, 3.635620872459002*^9}, 3.6356214175830727`*^9, 
   3.635621647868908*^9, 3.635622187782773*^9, 3.635622238556101*^9, {
   3.63562232164489*^9, 3.635622334300766*^9}, 3.6356226125565367`*^9, 
   3.6356246518942423`*^9, 3.6356249657373743`*^9, 3.6356250909691563`*^9, {
   3.635625214497645*^9, 3.635625235015666*^9}, 3.635625289781644*^9, 
   3.6356253264344788`*^9, 3.6357095324209967`*^9, 3.6357096273016644`*^9, {
   3.635709739940597*^9, 3.635709751563923*^9}, {3.635709781762677*^9, 
   3.6357097929112053`*^9}, 3.635709845577474*^9, {3.6357098795124607`*^9, 
   3.635709896672195*^9}, {3.635709935618533*^9, 3.63570994261799*^9}, {
   3.635710008824018*^9, 3.6357100202435217`*^9}, 3.635710107880259*^9, {
   3.635710139227652*^9, 3.63571016119531*^9}, 3.635710196744302*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"3000", ",", "b", ",", "5"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"b", ",", 
      RowBox[{"370", "*", 
       RowBox[{"10", "^", 
        RowBox[{"(", 
         RowBox[{"-", "9"}], ")"}]}]}], ",", 
      RowBox[{"750", "*", 
       RowBox[{"10", "^", 
        RowBox[{"(", 
         RowBox[{"-", "9"}], ")"}]}]}]}], "}"}]}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.635709459113037*^9, 3.635709518287682*^9}}],

Cell[BoxData["502125.48364185303`"], "Output",
 CellChangeTimes->{
  3.6357096273588037`*^9, {3.635709740077258*^9, 3.635709751616024*^9}, {
   3.635709781815548*^9, 3.6357097929543333`*^9}, 3.635709845615444*^9, {
   3.6357098795575123`*^9, 3.635709896713978*^9}, {3.6357099356576643`*^9, 
   3.6357099426649323`*^9}, {3.6357100088734694`*^9, 3.635710020284267*^9}, 
   3.635710107921784*^9, {3.635710139280323*^9, 3.6357101612519283`*^9}, 
   3.63571019680061*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{"502125.48364185303`", "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"5.67051", "^", 
   RowBox[{"(", 
    RowBox[{"-", "8"}], ")"}]}], "*", 
  RowBox[{"3000", "^", "4"}]}]}], "Input",
 CellChangeTimes->{{3.635709539038221*^9, 3.6357095903612623`*^9}}],

Cell[BoxData["502125.48364185303`"], "Output",
 CellChangeTimes->{
  3.635709591068138*^9, 3.635709627384171*^9, {3.635709740093709*^9, 
   3.6357097516200666`*^9}, {3.635709781820157*^9, 3.635709792958858*^9}, 
   3.635709845624791*^9, {3.635709879576316*^9, 3.635709896728528*^9}, {
   3.635709935673867*^9, 3.635709942680152*^9}, {3.63571000889159*^9, 
   3.635710020301813*^9}, 3.635710107939868*^9, {3.635710139296041*^9, 
   3.6357101612718773`*^9}, 3.6357101968235683`*^9}],

Cell[BoxData["7.577187839314447`*^7"], "Output",
 CellChangeTimes->{
  3.635709591068138*^9, 3.635709627384171*^9, {3.635709740093709*^9, 
   3.6357097516200666`*^9}, {3.635709781820157*^9, 3.635709792958858*^9}, 
   3.635709845624791*^9, {3.635709879576316*^9, 3.635709896728528*^9}, {
   3.635709935673867*^9, 3.635709942680152*^9}, {3.63571000889159*^9, 
   3.635710020301813*^9}, 3.635710107939868*^9, {3.635710139296041*^9, 
   3.6357101612718773`*^9}, 3.635710196824935*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", "[", "d_", "]"}], ":=", 
  RowBox[{
   RowBox[{"NIntegrate", "[", 
    RowBox[{
     RowBox[{"f", "[", 
      RowBox[{"d", ",", "b", ",", "5"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"b", ",", 
       RowBox[{"370", "*", 
        RowBox[{"10", "^", 
         RowBox[{"(", 
          RowBox[{"-", "9"}], ")"}]}]}], ",", 
       RowBox[{"750", "*", 
        RowBox[{"10", "^", 
         RowBox[{"(", 
          RowBox[{"-", "9"}], ")"}]}]}]}], "}"}]}], "]"}], "/", 
   RowBox[{"(", 
    RowBox[{"5.67051", "*", 
     RowBox[{"10", "^", 
      RowBox[{"(", 
       RowBox[{"-", "8"}], ")"}]}], "*", 
     RowBox[{"d", "^", "4"}]}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.635619836127984*^9, 3.635619886346095*^9}, {
  3.6356199576325283`*^9, 3.635619990899826*^9}, {3.635620281834053*^9, 
  3.6356203163052692`*^9}, {3.635620406180717*^9, 3.635620412348749*^9}, {
  3.6356204957356787`*^9, 3.635620498982177*^9}, {3.635620654764595*^9, 
  3.635620658110208*^9}, {3.635620867300971*^9, 3.6356208691923647`*^9}, {
  3.635621408764076*^9, 3.635621412262041*^9}, {3.635624634595771*^9, 
  3.6356246470660887`*^9}, {3.6356249574870577`*^9, 3.63562495759278*^9}, {
  3.635625196893606*^9, 3.635625231431836*^9}, {3.635625266954246*^9, 
  3.6356252716139307`*^9}, {3.6356253102679462`*^9, 3.635625318360853*^9}, {
  3.635709604337454*^9, 3.635709621882292*^9}, {3.635709911301175*^9, 
  3.6357099390830183`*^9}, {3.635709988832438*^9, 3.6357100053918343`*^9}, {
  3.6357101152118673`*^9, 3.635710157660343*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"g", "[", "d", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"d", ",", "3000", ",", "10000"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.635620875199361*^9, 3.635620879070751*^9}, {
  3.635621066535686*^9, 3.63562111905899*^9}, {3.635621426971573*^9, 
  3.6356214623812113`*^9}, {3.635621572669896*^9, 3.635621638469121*^9}, {
  3.6356219874328012`*^9, 3.635621992702497*^9}, {3.635622418380537*^9, 
  3.6356225470341063`*^9}, {3.635622639567737*^9, 3.6356226924086437`*^9}, {
  3.635624975022245*^9, 3.635625005723852*^9}, {3.635625072285069*^9, 
  3.6356251433580103`*^9}, {3.6357101837136173`*^9, 3.635710192793283*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV1nk8VdsXAHAkSaVI8cozRDylNPqZ90rSoDJESoVkaDJPIWRWSmSWSEKG
h8z3nnNNFykkZLrcw0MISSSK+O3+up/v55xzz9r7rL3WkrSwN7Di4uDgWMfJ
wfHnN8CbJsjhkAm2EdGtd5cINKw/pq3jkgn23AUTyptIpC0j7BV7JxMcvxzw
X5QiEW+Ly5C8XyZw7V0v+FCHRA8l9hVffJIJyse01AYTSRTNzDAqKs4ErwvB
RaYqDJSxNjre+mcmKHGsHXW/W4HexdiJNfu/Aq2ELa5CwVVo5CA9rCw3C4b2
fq24bcFEHh1cX4ZqsqFglVBVuEQ9OrP4wfppew4Ubtgkfq6nAc1lHNZ0WMwF
yQ1mUaHNjUjofJxAyMY8CO38MLE69D3iDXusSajkww8zLoOG0g9I7AWPtvq+
Ami0s7zJEG5DLhy1w+q0AvDRuLYv37AdFaeePtR85jVky6nytSd9RPm17d7e
3a8hi9/ixOrKDtSY9eycs3khOGYu3jrZ2IlaWw+w5mcLQbCOt9ewtws98ao3
eeZdBPZT/95sru9GGtd3PmwVLAadS3dsidc9iE/4zUmd9GJwz+7vawhgocjj
Npwje0pgtqqv887hXrSKaLuxsaYEVL9U1+QN9qJ9BWpNF0+XgrSnmZ+PUx86
PubYf2CwFDr1azq8F/uQ7+BnH33XMghzrHTKsWcjO1LX2829DM6ktqeoOrKR
aWyJ11OPMlgscut958RGGqf87nzyLoOog8K+I65stPxaxMkjuAy+eQfcFbzL
Rnf9jlulxpXBLg1FN/VQNrojka7zjVYGf40pxw+ksJGTqblI5HIZVJuL61g1
4fdZ3dcf5iiHzHsOsl3NbHTjduGD/60qh6+Vq+aOt7CRuSf3b/aacihu7bKU
aWOjs7Gv/tslUA6j9x886+xiI7n309m1UuUQo9OVzj3ERgPqfuoLJ8ohnQzL
il9gI92/X1iYRZWDocIP3X8lKGQgViQeFlMOk2EhMm8lKWQoXttXGlcO2/5j
/T28g0IXJEfO8yeVQ35TqL3wTgpZ7Nylw3hZDnPVOi+d5Sjktqfw4PbScqhW
eevNdYBCSeo13F3d5fB3gKCWlyaFxq4MZpwVo4Gro1r1kDmFZm9Ie9pL0GDO
rKW+9iqFll2tz0TsoMHTLD/OdAsKCYWPz7bK0KD19tLWa5YUQhUzYKRAA8fi
c286bCgUK7aq9xLQoCG83SrGjkJa/VIbb1jQQPZ2YJ+PF4V0J6wG71vS4HuI
Wr/2XQqZzGeWZFvTYH6kQ5Tfm0IOG+UvT96kwZ0yy5OJPhR6ig5l2TvToC5u
6XaWH4VmUrSOugXS4LiEJU9aCIVSza3cAzJoMBjkWMsdRaHyhJtBba9ocKyT
cZmJ/b7NPkoyhwbrBbo2+0VTaEnLM78ynwZ+Cpf6f8ZQ6Lzc49HFchrUS26d
GYinEN9MubFLIw2SlAuVg5Ip5BSwXslqmgZhccsTslkUCiUFtItnaJApF9ny
DjtlbqvhqjkaLNzT6rmdTaEmG0mH1J80+Gg9b5aXQyGZ04qv2Fx0aP8wA7J5
FOoVMhc5v4UOk2JXAqcL8f5kFC1oq9Ah7adO1iCBv1/P8wd2anTYtVX89VWS
Qr7rw0VjNehw82IY5wA23ek6fNKkA3Pn5F0Wg0L70N+hATp0+Gt2frC+kkKi
3cHCVZfpoNXnHuPNpND3tSaKyr500OtxY197RyFB9eMN5n50KMzKPdeEreBw
yCQ0gA7ZzpdFDjVS6GYnv09XCB3uKn56xNlEoYEXzDrXCDrQA2h7o5vx+lT3
GBWm0qGbw8Ah/gOF0mw5XHbX0sHD55WyWCe+3lwAfvV0WOaPrXDH/rHn6oau
BjrYmnZ7fMA+MVWd4d9MB0+l49G+XRSatAvo6enEz0eD2cduCik68KDQz3SQ
WZH2u9BLIbMPpeuoCfx86K/gdOzQfTbdB6fo0LW/rf4bNmv6jWP/DB3WOTjN
hfTh/XK8/1JxiQ6vBPuFctgUanBaz/eJn4D+lPjHTf0U+tZGdqoIEPC8cUVv
wwCFth20TYvYTICt5ya9M9i3Z5vV1EQIeGsqzNuILeDyGCcQAUv1ItaM/3C+
ugp2HDlEwI7hqvU+QxQK6KhJjVMkQNqfo7YQO/ews90XJQL2vXxAjmAvz7Wv
SVAnQKbJyun0MN4vtxiVaW0CXGPuz/N/wut3F3mefJEAds9KgsMIhcSGfvp/
vkTA+otezHhs3bO9VodMCTALSlGqwi6UTt79zoIAITmCZ8MohTxapcrmbhHQ
ya2glIydrbY6EewIyPHvr63C7sscuRvmQIBH6HjyIDb4ZGlKuhKgnzimKj1G
IZ7dCu9P+xCQZmYnnoKtFLvpddw9ArZ19ImS2Dc5Z6IG/QkIXja60Y3d3FV8
8U4IAcacMM3/Ga9PM1aNeZ8A/yjR3XLY+/LcxfkfEiCv01uiiR0dqPIpLYIA
Tb1Owhm7/uv2hqknBIyK+fwvDHve5He2cgwBzlv6V73ANtlf5dCSQECyQtrj
ZmxJ6tjSrxcERAYIeIiO43p4Urb/WDoB9/aY39uLHVjMWxORSUBAoRALYY89
aAyRySWgVTwoyBx724/cm455BNj153TZY+tcDT9DFhCQ8g+/tw923v/0NxuU
EPCr1KQmYfxPvh/4kVRGAC0h3yIDW5BfqGeURoDqkS3GhdhaHnPEAZIAgfGq
FAa263BnsncF3r+3o2oN2Bm65X4NVQRw81fubcPupidYbmYS0MX2cu3FVou8
vCvrDQF6sZ5ck9h2S+obvr/F8WTKnJzFTrERn9ZoIqAj5uGnn9gf2jja778n
oNfiv64VbC6NwZKPHwjYtdZEYvUEhasjM168nYD3Xrsr12JbC6V73ezA+ZLx
oHgDdrxvsGlJFwF8trkcAthvx22OcLAIuJNbl7YZ+5fRSWmdPgKGldclb8GW
r961JpYiYG7w9det2Kby68cHBgigbKfihbEj4r407R4iYH/CXMIf13C15Lt9
IkBCdGnmz/2ztgVPqkcJEMtQfPnn/6R7Il3Xj+N4+3v+/fO+81rOF4wnCSi0
2bbpTzzhWWqVnlMEzKvTePmx3/DzyCRPE6DyWsCBD5vDpeVh9QwBqdfOavNg
K/XEzw5/J8DmW0IYJ7aDhoUJ7zwBst/EYAnvT1ba7urdPwkImV1j+QN7kHdO
VneRALcA3/mvf/LBriLc6TcBvDIFv8b+5FN7yFzMCj4fJrW2/2GHKelfpnGS
sCNxRK8He4lrWI6Dh4ToVSs2b7APX/83QoqXhGGuU0l/vr9ds9u8Nh8JV8pP
HynC7o9bW/eIn4RzBTuoJGyR3227X28iQT7hVGcEtp5F0pOPgiR8DWxHgdg1
8grm24VJGHJ4bXMD+1fkQr3GXyTwjJnIXMI+OF+9x2I7CT91+Y1PY7+sNlx8
JU5CTrLqxj3YfTJiFk2SJDxrWhv353xseTja8FWKBJbHg8h12MHnPWMV/yHh
abJO/yg+T9cnUvbX7iOhq8tqMAH7ud6N+NEDJNyeNucOwu4pObDCd5iE8EDT
RHvsU/fqmvSVSajL5pU6ir1ny4RN/xESRK7n5gzhemDtWfyeS4uE9Zy6Tu+w
k/u9D8tok+D0SDOvAHtT9qZVtqdIkDnBDPbCntFQTP5lQEKQBa2CD3vXyxVu
MSMSJinHQ19wfbq29u2tI8Yk9Kg3SLRgf2y/pBx6iYRr6d+vRGKXXvf7uMWS
hIjmj3Ybsb2eNPHtdyVhdeIQ/wyup1XSF3P93UkIOLihrgGbu+zTmY8e+Plt
RX3J2A97lyPcfUjYGXhs/0nsJOn9whUhJGS80XCNxfW5ojRGSieRhPPyirGb
cf3mOrmjLimJhC+e6XcHBimk3ZtnPZVMQvJY+5tc7JaVN1mRaSTcSVF/dBR7
4MQvhe5cEgZ+TfXcwP2Cq9dUzaqSBP3fK5kJuP9o206wS6tJWPFJmbmM/WDF
3Ze3loRHvErp4tibpSNrchpIoDtKHk2j8PmxZZ6YaSVB4LyDTRruZ9orska+
wyRM6Jb6hOD+90Dqm23CWgaM21TrDOD+2pFvFcVcxwA9D7+jsdgSaqzyLxsY
4GJ2mtTBLjNkrtIUZIDwUPr+kg4KDQfFJIxvY4Bqc5+C/0cKqY8p16nKM+AL
98Qz7jYKTef6i1JnGZCAEi7l4nlAVXn+CK8+A+SV1ZkG2MF1t2wOnGPAOZWF
ggU8P4hShoXBxgy4v2AgfhT75EbZEwrmDDhjOK/T9pZCLxwbne85MkAXmGtY
9RQyUhRq3BHNgM3c/4hFVFHoVTONRzOWAb1Tyely2EuWZppX4xnw1TCHrMHz
zcuobFpKEgO4w4QTZytwPk3DK9F0BsAEbNXD81BEjm3Q1lIGlEf3HvxOo9A7
iTeIr5sBsn0TXON43oJ1nsXftlfAPZGhlqg0XG9bBnchsQpQ8onVW4O9HKWT
+lCiAhhq/saeLyjEEP07XHZnBUQzSxZMUymksrfK5sreCpBk2T/akYLrgT7P
9rdQASLVsrJRiTif4574PbeqgBC5W/rbI/F5lMo5q5tfAb+4uze24nk3g2QN
NitVQl5RTaK5Ae5vCR2yvc2V0LikGikjhucHL5uiQPkqkDO7MCk0xkbdYasT
KhyqoNtxyTY6j4081Nd2VdKrINBwLurfW2y0oLD+SetKFahwb2i4LcdGk0yX
9OxT1fDXcNqar+w+NF2+7CMTXQ2p3WGuARF9qG0qpKOwuxpqq4ejio71Ibl6
t0AeqRoIPScdITbTi0wpy4Kb12uAUWBVu+VFL0ozdnnhmFsDzLhUTW6DXnTF
6fMk+lEDjjUn1j3/yUJzzVeKxVSZcHD5jORcOgvBMw9lAzUmFNeMHPN+yUIP
bsdUBKkz4X3jugur01hIYl1zwyRigqSD5rEtz1no1Ak1Nl2LCfwL548dSmSh
5zXbeIzPMuFuYf6EXTi+XtZl/PgqE5Z/W5/ucmOh6ODZvhoLJoi0tGldcWWh
fqONFj+uMUFheEl+yJmFXL5r375izYRbLh+aphxYKGV/6b3dt5gwscbw9Opb
LPQ9JzrrjQsTjvnmX1QwYyHkVbB30ZUJMe+WjIqv4HhPNRXtdWeC43fho8qX
WUj886qKGA8mqM3++qp5Eccj49x2zYcJmwuPxxqdw/HMhRvH+TJh3YHubSx9
HE9tdt+7e0xI3URGmOrheK4NjuwPYIKEYpWezRkWqjywfMsqkAni0ZxPJ3RY
iI9r27f4ICbM+HR32p9iIcPWw25NwUzY4GzE/f0Ejv+5/uJKCBNqR0Kl7hxn
of8Dpne08A==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{3000., 0.09},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{"DefaultBoundaryStyle" -> Automatic, "ScalingFunctions" -> None},
  PlotRange->{{3000, 10000}, {0.10932140461642499`, 0.48309888873190504`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.635625327420583*^9, 3.635709533453981*^9, 3.635709627692137*^9, {
   3.6357097404290323`*^9, 3.635709751920195*^9}, {3.635709782120075*^9, 
   3.635709793260236*^9}, 3.635709845915688*^9, {3.635709879887451*^9, 
   3.635709897028288*^9}, {3.635709936021628*^9, 3.635709943363545*^9}, {
   3.63571000919744*^9, 3.635710020611158*^9}, 3.6357101082416897`*^9, {
   3.635710140100739*^9, 3.6357101615810738`*^9}, 3.6357101977620707`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"N", "[", 
   RowBox[{"g", "[", "2403", "]"}], "]"}], "*", "100."}]], "Input",
 CellChangeTimes->{{3.6357097051043797`*^9, 3.635709789272957*^9}, {
  3.6357098703272552`*^9, 3.6357098726511927`*^9}, {3.635710093204502*^9, 
  3.635710100753354*^9}}],

Cell[BoxData["3.953907224141501`"], "Output",
 CellChangeTimes->{{3.635709722760336*^9, 3.635709751958001*^9}, {
   3.6357097821573257`*^9, 3.6357097932978077`*^9}, {3.635709845951893*^9, 
   3.635709897057374*^9}, {3.635709936062791*^9, 3.635709943440954*^9}, {
   3.635710009236882*^9, 3.635710020652471*^9}, 3.635710108287318*^9, {
   3.635710140173637*^9, 3.635710161631134*^9}, 3.6357101978008957`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"5.67051", "*", 
    RowBox[{"10", "^", 
     RowBox[{"(", 
      RowBox[{"-", "8"}], ")"}]}], "*", 
    RowBox[{"d", "^", "4"}]}], ")"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{3.635710103542809*^9}],

Cell[BoxData[
 RowBox[{"5.67051`*^-8", " ", 
  SuperscriptBox["d", "4"]}]], "Output",
 CellChangeTimes->{{3.635710104085832*^9, 3.635710108292136*^9}, {
   3.635710140194922*^9, 3.635710161636663*^9}, 3.635710197805121*^9}]
}, Open  ]]
},
WindowSize->{1364, 709},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (December 4, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1368, 37, 77, "Input"],
Cell[CellGroupData[{
Cell[1951, 61, 543, 16, 32, "Input"],
Cell[2497, 79, 1600, 31, 74, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4134, 115, 275, 7, 32, "Input"],
Cell[4412, 124, 1524, 28, 72, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5973, 157, 535, 16, 32, "Input"],
Cell[6511, 175, 466, 7, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7014, 187, 264, 6, 55, "Input"],
Cell[7281, 195, 480, 7, 32, "Output"],
Cell[7764, 204, 480, 7, 32, "Output"]
}, Open  ]],
Cell[8259, 214, 1552, 35, 32, "Input"],
Cell[CellGroupData[{
Cell[9836, 253, 684, 12, 32, "Input"],
Cell[10523, 267, 6732, 120, 275, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17292, 392, 282, 6, 32, "Input"],
Cell[17577, 400, 407, 5, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18021, 410, 266, 8, 55, "Input"],
Cell[18290, 420, 223, 4, 32, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
