reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'wien.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 9 ps 1.5
set style line 9 lc rgb '#000f0f' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 4 lc rgb '#000000' lt 1
set border 3 back ls 4
set tics nomirror out scale 0.5
# Grid
set style line 5 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 5


#Legenda
set key top center title 'Legenda' box 5

#axis labels
set xlabel '$\log(T)$'
set ylabel '$\log(\lambda)$' #rotate by  90 center 


set bars 2

#fit linear
lin(x)=a+b*x
a=3;b=-1;
fit lin(x) 'wien.txt' via a,b

#set label 1 '$log(U)=(5.31 \pm 0.05)\log(T)+(-15.6\pm 0.2)$' at 0.55,0.85 ls 1

#rotate by 34 center tc 

set xrange [3.29:3.4]
set yrange [-6.2:-5.95]
plot 'wien.txt' u 1:2:3:4:5:6 notitle w xyerrorbars ls 1, \
     lin(x) title '$log(\lambda)=(-1.3\pm0.3)\log(T)+(-1.6\pm0.6)$' ls 2
