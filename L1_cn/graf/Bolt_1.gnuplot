reset

#epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Bolt_1.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 1.5
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 1.5
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb '#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha


#Legenda
set key top right  title 'Temperatura' box 5


#axis labels
set xlabel '$\lambda/ nm$'
set ylabel '$U_{det}$' #font ",20"

set bars 2 #espessura das barras de erro

#fits
noveV(x)=b_2/((x+a)**5*(exp(c_2/(x+a))-1))

fum(x)=2*h*c**2/((x*10**(-9))**5*(exp(h*c/(k*2173*(x*10**(-9))))-1))/((2*c**2*h*2173**5)/(b**5*(-1 + exp((c*h)/(b*k)))))

b=0.0028977721;
c=299792458;
k=1.380658*10**(-23);
h=6.626*10**(-34);
p=3.141592654;

#a_1=-217;a_2=-217;a_3=-217;
a=-213.4;
#a_1=0;a_2=0;a_3=0;
b_1=10**19;b_2=10**19;b_3=10**19;
c_1=4*10**3;c_2=4*10**3;c_3=4*10**3;

fit noveV(x) '2173.txt' u 1:($2/0.6) via b_2,c_2,a

g(x)=fum(x-a)

set xrange [400:6000]

plot '2173.txt'  u 1:($2/0.6):3:4:(($2-$5)/0.6):(($2+$5)/0.6) title "$2173\\\pm2~K$" w xyerrorbars ls 3, \
     noveV(x) notitle ls 3, \
     g(x) title "Curva te\\\'{o}rica" ls 5
