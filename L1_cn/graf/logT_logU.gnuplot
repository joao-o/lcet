reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'logT_logU.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5


# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12

#Legenda
set key top left vertical title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel '$\log(T)$'

set tics scale 2

set bars 2 #espessura das barras de erro

max(x)=a+b*x
fit max(x) 'logT_logU.txt' u 1:2 via a,b

set label 1 '$log(U)=(5.31 \pm 0.05)\log(T)+(-15.6\pm 0.2)$' at 0.55,0.85 rotate by 34 center tc ls 1
######
#set xrange [0.1:1.3]
#set yrange [0.5:1.3]
#set ylabel '$\log(U)$)' #rotate by  90 center 
plot 'log(T)_log(U).txt' u 1:2:3 notitle w yerrorbars ls 2, \
      max(x) notitle ls 1
