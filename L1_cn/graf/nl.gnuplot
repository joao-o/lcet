reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'nl.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 9 ps 1.5
set style line 9 lc rgb '#000f0f' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 4 lc rgb '#000000' lt 1
set border 3 back ls 4
set tics nomirror out scale 0.5
# Grid
set style line 5 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 5

#linhas dos pontos com as resistências medidas
set style line 6 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 7 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 8 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
#set arrow from 14.58766,0 to 14.58766,2416.92 nohead ls 6
#set arrow from 12.63005,0 to 12.63005,2172.80 nohead ls 7
#set arrow from 11.33346,0 to 11.33346,2002.92 nohead ls 8
#set arrow from 0,2416.92 to 14.58766,2416.92 nohead ls 6
#set arrow from 0,2172.80 to 12.63005,2172.80 nohead ls 7
#set arrow from 0,2002.92 to 11.33346,2002.92 nohead ls 8

#Legenda
set key top right title 'Legenda' box 5

#axis labels
set xlabel '$n$'
set ylabel '$\lambda/nm$' #rotate by  90 center 


plot 'tabela_nl.txt' u 1:2 title "$\\\lambda(n)$" with linespoints ls 3, \
