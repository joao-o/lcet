reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'adiabatica.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12


#Legenda
set key top left title 'Legenda' box 5

#axis labels
set xlabel '$\\log(V)$'
set ylabel '$\\log(P)$' #rotate by  90 center 


set bars 2

#fit linear
lin(x)=gamma*x+b
gamma=-1.4;b=0.8; 
fit lin(x) 'joaoadbPV.txt' using (log($1)):(log($2))


#plotS
plot 'joaodbPV.txt' u (log($1)):(log($2)) notitle ls 1, \
      lin(x) title sprintf('$\\log(P)=(%1.3f\pm%0.3f) \\log(V)+%1.3f\pm0.3f$',gamma,gamma_err,b,b_err) ls 1
