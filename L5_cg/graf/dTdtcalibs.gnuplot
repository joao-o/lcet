reset

# epslatex
set terminal epslatex size 16cm, 12cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'dTdtcalibs.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set style line 4 lc rgb '#16fb30' lt 1 lw 2 
set style line 6 dt 1 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 7 dt 1 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set key box at 35,0.6 spacing 5 nobox title 'Legenda'

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12
set ytics nomirror
set y2tics 10 rotate by 90 center
set y2range[300:380]


#axis labels
set xlabel '$t(ms)$'
set ylabel '$s.d.$' offset 1.5
set y2label '$T~(K)$' offset -2.5

#liñas de calibracion
set arrow from 40,-0.4 to 40,0.6  nohead ls 6
set arrow from 73,-0.4 to 73,0.6 nohead ls 7

#labels for calibration points
set label 1 '$1^a~zona$' at 8,-0.05 tc ls 11
set label 2 '$2^a~zona$' at 55,0.05 tc ls 11
set label 3 '$3^a~zona$' at 77,0.43 tc ls 11

plot 'tcalib.txt' u 1:3 axes x1y1 title sprintf('\shortstack{$~\frac{\mathrm{d}T}{\mathrm{d}t}$}') ls 3, \
     'tcalib.txt' u 1:($4*50) axes x1y1 title sprintf('\shortstack{$~\frac{\mathrm{d^2}T}{\mathrm{d^2}t}$}') ls 4, \
     'tcalib.txt' u 1:2:(0.35):(1.37) axes x1y2 title '$~T$' ls 1
