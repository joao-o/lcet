reset

# epslatex
set terminal epslatex size 16cm,10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'adiabaticas.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#00FFD3' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#ranges
#set xrange [-2:10]
#set yrange [4.7:5.6]

#Legenda
set key top left title 'Legenda' nobox 

#axis labels
set xlabel '$\log(V)$'
set ylabel '$\log(T)$' #rotate by  90 center 


set bars 2

#fit linear
lin(x)=g*x+b
linn(x)=c*x+d
g=-0.4;b=0.8;
c=-0.4;d=0.8;
fit lin(x) 'adiabaticaTV.txt' using (log($2)):(log($4)) via g,b
fit linn(x) 'adiabaticaTV.txt' using (log($2)):(log($4)):(0.39*1/$2):(9*1/$4) xyerror via d


plot 'adiabaticaTV.txt' u (log($2)):(log($4)):(0.39*1/$2):(9*1/$4) title 'Dados' w xyerrorbars ls 2, \
      lin(x) title sprintf('$\log(T)=(%1.3f\pm %0.3f) \log(V)+%1.2f\pm %0.2f$',g,g_err,b,b_err) ls 1, \
      linn(x) title 'Reta com declive esperado' ls 3


