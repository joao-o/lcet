reset

# epslatex
set terminal epslatex size 16cm,10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'nmoles.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#00FFD3' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF00D3' lt 1 lw 1.5 pt 6 ps 2

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#ranges
#set xrange [-2:10]
set yrange [0.00755:0.0081]

#Legenda
set key top right title 'Legenda' nobox

#axis labels
set xlabel '$t(ms)$'
set ylabel '$n^o(mol)$' #rotate by  90 center 


set bars 2

#fit linear
lin(x)=b+a*x
const(x)=c
b=0.007767;
c=0.007;
a=0.01;
fit lin(x) 'nmoles.txt' using 1:2:(0.35):3 xyerror via a,b
fit const(x) 'nmoles.txt' using 1:2:(0.35):3 xyerror via c
 

plot 'nmoles.txt' u 1:2:(0.35):3 title '$n^o~mol(t)$' w xyerrorbars ls 2, \
      const(x) title "$n^o$ mol m\\\'{e}dio" ls 3, \
      lin(x) title "$n^o$ mol assumindo varia\\\c{c}\\\~{a}o linear" ls 1
