reset

# epslatex
set terminal epslatex size 16cm, 10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Tcalibs.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set style line 4 lc rgb '#16fb30' lt 1 lw 2 
set style line 6 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 7 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 8 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set key box at 35,375 nobox

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12
set ytics nomirror
set xrange [-1:101]
set y2tics 10 rotate by 90 center
set y2range[90:180]


#axis labels
set xlabel '$t(ms)$'
set ylabel '$T~(K)$' offset 1.5
set y2label '$V~(cm^3)$' offset -2.5

#liñas de calibracion
set arrow from -1,351.2 to 52.6,351.2  nohead ls 6
set arrow from -1,304.5 to 52.6,304.5  nohead ls 6
set arrow from 52.6,300 to 52.6,351.2  nohead ls 7
set arrow from -1,320.51 to 30.1,320.51  nohead ls 7
set arrow from 30.1,300 to 30.1,320.51  nohead ls 7

#labels for calibration points
set label 1 '$(52,60~ms;94,71~cm^3)$' at 55,312 tc ls 2
set label 2 '$(52.60~ms;351.2~K)$' at 55,348 tc ls 1
set label 3 '$(30.10~ms;320.5~K)$' at 42,323 tc ls 1


plot 'tcalib.txt' u 1:2:(0.35):(1.26) axes x1y1 title '$~T$' w xyerrorbars ls 1, \
     'pcalib.txt' u 1:2:(0.35):(1.37) axes x1y2 title '$~V$' w xyerrorbars ls 2, \
     'vatraso.txt'u 1:2:(0.35):(0.39) axes x1y2 notitle w xyerrorbars ls 6, \
     'tatraso.txt'u 1:4:(0.35):(1.3)  axes x1y1 notitle w xyerrorbars ls 6

