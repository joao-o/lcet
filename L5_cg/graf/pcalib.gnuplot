reset

# epslatex
set terminal epslatex size 16cm, 10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Tcalib.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set style line 4 lc rgb '#16fb30' lt 1 lw 2 
set key box

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12
set ytics nomirror
set y2tics 10 rotate by 90 center
set y2range[90:180]

#axis labels
set xlabel '$t(ms)$'
set ylabel '$T~(K)$' offset 1.5
set y2label '$V~(cm^3)$' offset -2.5


plot 'tcalib.txt' u 1:2 axes x1y1 title '$~T$' ls 1, \
     'tcalib.txt' u 1:(3*600) axes x1y1 title sprintf('\shortstack{$~\frac{\mathrm{d}T}{\mathrm{d}t}$}') ls 2, \
     'tcalib.txt' u 1:(4*80000) axes x1y1 title sprintf('\shortstack{$~\frac{\mathrm{d^2}T}{\mathrm{d^2}t}$}') ls 3, \
     'pcalib.txt' u 1:2:(0.35):(1.37) axes x1y2 title '$~V$' ls 4

#f(x,y) title sprintf('\shortstack{$k=(%3.1f\pm%1.1f)~WK^{-1}m^{-1} $\\ $T_h=(%2.2f\pm%0.2f)~K $\\ $T_c=(%2.2f\pm%0.2f)~K$}',k,k_err,Th,Th_err,Tc,Tc_err)
