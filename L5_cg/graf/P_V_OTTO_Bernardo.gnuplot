reset

#um sonho que por falta de dados teve de ser abandonado *tears* #soquenao

#epslatex
set terminal epslatex size 16cm,10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"

#set terminal x11

# color definitions
set border linewidth 1
set style line 1 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 1 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 1
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1
set style line 4 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 1
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 1
set style line 6 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

#set xrange [0:9]
#set xtics 1,1,9 #1st,step,last
#set logscale y
set yrange [0:400]
set xrange [0:350]
#Legenda
set key top right title 'Graficos P(V)' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set ylabel '$P/kPa$'
set xlabel '$V/cm^3$' #offset 2,0

set tics scale 2

set bars 2 #espessura das barras de erro


#desenho de area de gráfico teórico
set arrow from 234.05596,95.970690 to 234.05596,70.51054 nohead ls 6
set arrow from 95.6315,246.88645 to 95.6315,332.98 nohead ls 6

n=0.01128;
tin(x)=(95.6315<x&&x<234.05596)?199152.7022/x**1.4:1/0
tino(x)=(95.6315<x&&x<234.05596)?146331.7022/x**1.4:1/0

set samples 1000
set output 'P_V_OTTO_Bernardo.tex'
plot 'P_V_OTTO_Bernardo.txt' u 1:2:3:4 title "Dados"  ls 4, \
     tin(x)  title '$Ciclo~Otto~Real$'  ls 3, \
     tino(x)  notitle ls 3
