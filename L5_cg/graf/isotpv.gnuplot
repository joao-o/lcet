reset

# epslatex
set terminal epslatex size 16cm, 10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'isotPV.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set key box nobox


# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12
set ytics nomirror
set y2tics 30 rotate by 90 center
set y2range[100:300]

#axis labels
set xlabel '$t(ms)$'
set ylabel '$V~(cm^3)$' offset 1.5
set y2label '$P~(kPa)$' offset -2.5

plot 'isotpv.txt' u 1:2:(0.35):(0.39) axes x1y1 title '$~V$' ls 1, \
     'isotpv.txt' u 1:3:(0.35):(1.37) axes x1y2 title '$~P$' ls 3
