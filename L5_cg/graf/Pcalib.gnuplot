reset

# epslatex
set terminal epslatex size 16cm, 10cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Pcalib.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set style line 6 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 7 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set style line 8 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5
set key box at 110,150 nobox title 'Legenda'


# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12
set ytics nomirror
set y2tics 30 rotate by 90 center
set y2range[100:300]

#axis labels
set xlabel '$t(ms)$'
set ylabel '$V~(cm^3)$' offset 1.5
set y2label '$P~(kPa)$' offset -2.5

#liñas de calibracion
set arrow from 52.595,90 to 52.595,94.70522  nohead ls 6
set arrow from -20,94.70522 to 52.595,94.70522  nohead ls 6
set arrow from 53.633,90 to 53.633,167.5  nohead ls 7
set arrow from 120,167.7 to 53.633,167.5  nohead ls 7

#labels for calibration points
set label 1 '$(52,60~ms;94,71~cm^3)$' at 55,102 tc ls 1
set label 2 '$(53,63~ms;272,3~KPa)$' at 13,172 tc ls 3


plot 'pcalib.txt' u 1:2:(0.35):(0.39) axes x1y1 title '$~V$' w xyerrorbars ls 1, \
     'pcalib.txt' u 1:3:(0.35):(1.37) axes x1y2 title '$~P$' w xyerrorbars ls 3, \
     'vatraso.txt'u 1:2:(0.35):(0.39) axes x1y1 notitle w xyerrorbars ls 6, \
     'patraso.txt'u 1:3:(0.35):(1.37) axes x1y2 notitle w xyerrorbars ls 6
