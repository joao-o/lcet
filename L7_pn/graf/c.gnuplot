set terminal epslatex size 13.5cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'c.tex'

set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 2
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 2
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 2
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 2
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 2
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5

# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#define Ranges
set xrange [285:380]
set yrange [0.45:0.85]

set key top left title 'Legenda' nobox width -23
set key spacing 2
#axis labels
set xlabel '$T(K)$'
set ylabel '$c(T)$' #rotate by  90 center '$C(T)=(%1.3f \pm %0.3f) \frac{KT}{q}\log\left((1.3f \pm %0.3f)~T^3~e^\frac{-(%1.3f\pm %0.3f)}{KT}\right)$'


set bars 2

set dummy T

K=1.38e-23
q=1.602e-19
n=1.28
C(T)=-n*T*K/q*log(d*T*T*T*exp(-Eg/(K*T)))
d=300
Eg=1.7335e-19

fit C(T) "c.txt" us 1:3:2:4 xyerrors via n,d,Eg

plot "c.txt" us 1:3:2:4 title '$Pontos~~Experimentais$' w  xyerrorbars lc rgb '#0000FF' pt 21 ps 0.5, \
     C(T) title sprintf('$C(T)=-\frac{(%1.2f \pm 0.08) KT}{q}\log((4\pm 2)\times 10^{2}T^3e^\frac{-(1.83\pm 0.09)\times 10^{-19}}{KT})$',\
     n,Eg) ls 3
     
