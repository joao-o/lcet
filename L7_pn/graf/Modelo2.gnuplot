set fit results
set fit limit 1e-16

set dummy I
Const=2*1.38e-23/(1.69e-19)

UA(I)=RA*I+Const*(273.15+TA)*log(sqrt(((irA*irAs)/(2.0*isA*isAs))**2.0+(I+irA*irAs)/(isA*isAs)+1)-irAs*irA/(2.0*isA*isAs))
isAs  = 1e-16
irAs  = 1e-7
isA   = 4.908366
irA   = 3.22279  
RA    = 5.43585e-1 
TA    = 25.0
fit UA(I) "../dados/25.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irA,RA,isA

UB(I)=RB*I+Const*(273.15+TB)*log(sqrt(((irB*irBs)/(2.0*isB*isBs))**2.0+(I+irB*irBs)/(isB*isBs)+1)-irBs*irB/(2.0*isB*isBs))
isBs  = 1e-10
irBs  = 1e-6
isB   = 1
irB   = 1  
RB    = 3.43585e-1 
TB    = 45.588
fit UB(I) "../dados/42_588.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irB,RB,isB

UC(I)=RC*I+Const*(273.15+TC)*log(sqrt(((irC*irCs)/(2.0*isC*isCs))**2.0+(I+irC*irCs)/(isC*isCs)+1)-irCs*irC/(2.0*isC*isCs))
isCs  = 1e-10
irCs  = 1e-6
isC   = 4.908366
irC   = 3.22279  
RC    = 3.43585e-1 
TC    = 53.35
fit UC(I) "../dados/55_35.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irC,RC,isC

UD(I)=RD*I+Const*(273.15+TD)*log(sqrt(((irD*irDs)/(2.0*isD*isDs))**2.0+(I+irD*irDs)/(isD*isDs)+1)-irDs*irD/(2.0*isD*isDs))
isDs  = 1e-8
irDs  = 1e-6
isD   = 4.908366
irD   = 3.22279  
RD    = 3.43585e-1 
TD    = 71.5365
fit UD(I) "../dados/71_5365.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irD,RD,isD

UE(I)=RE*I+Const*(273.15+TE)*log(sqrt(((irE*irEs)/(2.0*isE*isEs))**2.0+(I+irE*irEs)/(isE*isEs)+1)-irEs*irE/(2.0*isE*isEs))
isEs  = 1e-8
irEs  = 1e-6
isE   = 4.908366
irE   = 3.22279  
RE    = 3.43585e-1 
TE    = 85.600
fit UE(I) "../dados/85_600.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irE,RE,isE

UF(I)=RF*I+Const*(273.15+TF)*log(sqrt(((irF*irFs)/(2.0*isF*isFs))**2.0+(I+irF*irFs)/(isF*isFs)+1)-irFs*irF/(2.0*isF*isFs))
isFs  = 1e-8
irFs  = 1e-6
isF   = 4.908366
irF   = 3.22279  
RF    = 3.43585e-1 
TF    = 101.35
fit UF(I) "../dados/101_35.txt" us ($1*1e-6):2:($3*1e-6):4 xyerrors via irF,RF,isF


set terminal epslatex size 13.5cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'Modelo2.tex'

set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 3 ps 1.5
set style line 2 lc rgb '#FF0000' lt 1 lw 1.5 pt 3 ps 1.5
set style line 3 lc rgb '#00BFFF' lt 1 lw 1.5 pt 3 ps 1.5
set style line 4 lc rgb '#228B2F' lt 1 lw 1.5 pt 3 ps 1.5
set style line 5 lc rgb '#E9967A' lt 1 lw 1.5 pt 3 ps 1.5
set style line 6 lc rgb '#FFD700' lt 1 lw 1.5 pt 3 ps 1.5

set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 

set grid back ls 12 
set tics nomirror out
set key top left title nobox
set border 3 back ls 1
set tics nomirror out scale 1

set logscale x
set format x '$%.0l\times10^{%L}$'
set xrange [7.5e-6:1.5e-1]
set mxtics 10
set xlabel "$I(A)$"

set ylabel "$U(V)$"

plot "../dados/25.txt" us ($1*1e-6):2:($3*1e-6):4 notitle  w \
       xyerrorbars ls 1,\
       UA(I) title "T=298K" ls 1,\
     "../dados/42_588.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 2,\
       UB(I) title "T=315K" ls 2,\
     "../dados/55_35.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 3,\
       UC(I) title "T=328K" ls 3,\
     "../dados/71_5365.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 4,\
       UD(I) title "T=344K" ls 4,\
     "../dados/85_600.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 5,\
       UE(I) title "T=358K" ls 5,\
     "../dados/101_35.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 6,\
       UF(I) title "T=374K" ls 6 
