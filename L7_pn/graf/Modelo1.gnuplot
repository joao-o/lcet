#comments started with '#'

set terminal epslatex size 18cm,12cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'ErsteModel.tex'

set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 2
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 2
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 2
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 2
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 2
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5

# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#define Ranges
x_min="0"; x_max="6500"
y_min="0"; y_max="1.05"

set key top left Left title 'Legenda' nobox

#axis labels
set xlabel '$log(I)$'
set ylabel '$V~(V)$' #rotate by  90 center 


set bars 2

set dummy I


Uv(I)=Rv*exp(I)+bv*I+cv
bv  = 0.0469104  
cv  = 0.714594   
Rv  = 0.438042   

fit Uv(I) "V0.txt" us (log($1)):2:($3/$1):4 xyerrors via bv,cv,Rv

U46(I)=R46*exp(I)+b46*I+c46
b46  = 0.0485394  
c46  = 0.671501   
R46  = 0.908089   

fit U46(I) "V9.txt" us (log($1)):2:($3/$1):4 xyerrors via b46,c46,R46

U59(I)=R59*exp(I)+b59*I+c59
b59  = 0.0485431 
c59  = 0.630505   
R59  = 0.831728   

fit U59(I) "V12.txt" us (log($1)):2:($3/$1):4 xyerrors via b59,c59,R59

U75(I)=R75*exp(I)+b75*I+c75
b75  = 0.0477562  
c75  = 0.587577   
R75  = 0.959  

fit U75(I) "V15.txt" us (log($1)):2:($3/$1):4 xyerrors via b75,c75,R75


U90(I)=R90*exp(I)+b90*I+c90
b90  = 0.0474 
c90  = 0.552   
R90  = 0.9499   

fit U90(I) "V17.txt" us (log($1)):2:($3/$1):4 xyerrors via b90,c90,R90

U100(I)=R100*exp(I)+b100*I+c100
b100  = 0.0474 
c100  = 0.552   
R100  = 0.9499   

fit U100(I) "V20.txt" us (log($1)):2:($3/$1):4 xyerrors via b100,c100,R100

plot "V0.txt" us (log($1)):2:($3/$1):4 title '$(298.2\pm 0.2)K$' w  xyerrorbars lc rgb '#0F0F0F' pt 21 ps 0.5, \
     Uv(I) notitle,\
     "V9.txt" us (log($1)):2:($3/$1):4  title '$(315.8\pm 0.2)K$' w  xyerrorbars lc rgb '#FF0000' pt 21 ps 0.5,\
     U46(I) notitle,\
     "V12.txt" us (log($1)):2:($3/$1):4  title'$(328.5\pm 0.2)K$' w  xyerrorbars lc rgb '#00BFFF' pt 21 ps 0.5,\
     U59(I) notitle,\
     "V15.txt" us (log($1)):2:($3/$1):4 title '$(344.7\pm 0.4)K$' w  xyerrorbars lc rgb '#FF00FF' pt 21 ps 0.5,\
     U75(I) notitle,\
     "V17.txt" us (log($1)):2:($3/$1):4  title '$(358.8\pm 0.1)K$' w  xyerrorbars lc rgb '#228B22' pt 21 ps 0.5,\
     U90(I) notitle,\
     "V20.txt" us (log($1)):2:($3/$1):4  title '$(374.5\pm 0.5)K$' w  xyerrorbars lc rgb '#E9967A' pt 21 ps 0.5,\
     U100(I) notitle
     


