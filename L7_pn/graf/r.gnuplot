#comments started with '#'

set terminal epslatex size 13.5cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'r.tex'

set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 2
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 2
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 2
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 2
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 2
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5

# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#define Ranges
x_min="0"; x_max="6500"
y_min="0"; y_max="1.05"


#axis labels
set xlabel '$T(K)$'
set ylabel '$R(\Omega)$' #rotate by  90 center 


set bars 2

set dummy T

plot "r.txt" us 1:3:2:4 notitle w  xyerrorbars lc rgb '#0000FF' pt 21 ps 0.5
