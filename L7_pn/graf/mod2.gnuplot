print "---mod2---"
set fit results
set fit limit 1e-16
set dummy T

Irs(T,y)=(y==1) ? krs*(T**(5/2))*exp(-eg/2/1.38e-23/T) : ksr*(T**3)*exp(-eg/1.38e-23/T);

eg=1e-19
krs=1
ksr=1
fit Irs(T,y) "../dados/reg2.txt" us 1:4:2:5:($3*$2) errors T,z via krs,ksr,eg

# select the output
set terminal epslatex size 13.5cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output "mod2.tex"
#set terminal x11

# color definitions
set border 3 linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 1.5
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 1.5
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 1.5
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 1.5
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 1.5

set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 
set grid back ls 12 
set tics nomirror out

set logscale y
set yrange [.5e-12:1e-4]
set format y '$%.0l\times10^{%L}$'
set mytics 10 
set ylabel "$I(A)$"

set xrange [290:400]
set xlabel "$T(K)$"

set key bottom right spacing 2 

plot "../dados/reg2.txt" every ::0::5 us 1:2:5:($3*$2) notitle w \
       xyerrorbars ls 6,\
       Irs(T,1) ls 6 title sprintf('$I_r(T)=k_rT^\frac{5}{2}\it{exp}\left(\frac{-E_g}{2KT}\right)$'),\
     "../dados/reg2.txt" every ::6::12 us 1:2:5:($3*$2) notitle w \
       xyerrorbars ls 7,\
       Irs(T,2) ls 7 title sprintf('$I_s(T)=k_sT^3\it{exp}\left(\frac{-E_g}{KT}\right)$')

set output "rm2.tex"

set nologscale
set xrange [*:*]  

set yrange [0:2]
set nomytics 
set format y '$%.1f$'
set ylabel '$R (\Omega)$'

plot "../dados/reg.txt"  us 1:3:($3*$6) notitle w \
       yerrorbars lc rgb '#0000EE' pt 21 ps 0.5
