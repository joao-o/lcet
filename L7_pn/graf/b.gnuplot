#comments started with '#'

set terminal epslatex size 18cm,12cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'B(T).tex'

set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0F0F0F' lt 1 lw 1.5 pt 6 ps 2
set style line 3 lc rgb '#FF0000' lt 1 lw 1.5 pt 6 ps 2
set style line 4 lc rgb '#00BFFF' lt 1 lw 1.5 pt 6 ps 2
set style line 5 lc rgb '#FF00FF' lt 1 lw 1.5 pt 6 ps 2
set style line 6 lc rgb '#228B22' lt 1 lw 1.5 pt 6 ps 2
set style line 7 lc rgb '#E9967A' lt 1 lw 1.5 pt 6 ps 2
set style line 8 lc rgb '#FFD700' lt 1 lw 1.5 pt 6 ps 2
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5

# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12

#define Ranges
x_min="0"; x_max="6500"
y_min="0"; y_max="1.05"

set key top left Left title 'Legenda' nobox

#axis labels
set xlabel '$T~(K)$'
set ylabel '$b(T)~(V)$' #rotate by  90 center 


set bars 2

set dummy T

K=1.38e-23
q=1.69e-19

B(T)=n*T*K/q
n  = 1.2

fit B(T) "b.txt" us 1:3:2:4 xyerrors via n

plot "b.txt" us 1:3:2:4 title '$Pontos~~Experimentais$' w  xyerrorbars lc rgb '#0000FF' pt 21 ps 0.5, \
     B(T) title sprintf('$B(T)=-\frac{(%1.2f \pm 0.08) KT}{q}$',\
     n,err_n) ls 3
     


