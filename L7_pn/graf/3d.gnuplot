set fit limit 1e-16
set dummy I,T
Const=2*1.3806488e-23/1.602176565e-19

UA(I,T)=R*I+Const*T*log(sqrt(((k1*(T**(5/2))*exp(-eg/2/K/T))/(2.0*k2*(T**3)*exp(-eg/K/T)))**2.0+(I+k1*(T**(5/2))*exp(-eg/2/K/T))/(k2*(T**3)*exp(-eg/K/T))+1)-k1*(T**(5/2))*exp(-eg/2/K/T)/(2.0*k2*(T**3)*exp(-eg/K/T)))
R    = 1
K    = 1.3806488e-23
k1   = 0.05
k2   = 86
eg   = 1.4e-19

fit UA(I,T) "../dados/3d.txt" us ($1*1e-6):5:2:($3*1e-6):6:4  errors I,T,z via R,eg,k1,k2


set terminal epslatex size 13.5cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '3d.tex'

set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 3 ps 1.5
set style line 2 lc rgb '#FF0000' lt 1 lw 1.5 pt 3 ps 1.5
set style line 3 lc rgb '#00BFFF' lt 1 lw 1.5 pt 3 ps 1.5
set style line 4 lc rgb '#228B2F' lt 1 lw 1.5 pt 3 ps 1.5
set style line 5 lc rgb '#E9967A' lt 1 lw 1.5 pt 3 ps 1.5
set style line 6 lc rgb '#FFD700' lt 1 lw 1.5 pt 3 ps 1.5

set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 

set grid  back ls 12 
set tics nomirror out
set key top left title nobox
set border 3 back ls 1
set tics nomirror out scale 1

set logscale x
set format x '$%.0l\times10^{%L}$'
set xrange [7.5e-6:1.5e-1]
set mxtics 10
set xlabel "$I(A)$"

set ylabel "$U(V)$"

plot "../dados/25.txt" us ($1*1e-6):2:($3*1e-6):4 notitle  w \
       xyerrorbars ls 1,\
       UA(I,298) title "T=298K" ls 1,\
     "../dados/42_588.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 2,\
       UA(I,315) title "T=315K" ls 2,\
     "../dados/55_35.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 3,\
       UA(I,328) title "T=328K" ls 3,\
     "../dados/71_5365.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 4,\
       UA(I,344) title "T=344K" ls 4,\
     "../dados/85_600.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 5,\
       UA(I,358) title "T=358K" ls 5,\
     "../dados/101_35.txt" us ($1*1e-6):2:($3*1e-6):4 notitle w \
       xyerrorbars ls 6,\
       UA(I,374) title "T=374K" ls 6
