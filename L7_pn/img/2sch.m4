% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  3.6                          # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 0.8); llabel(,"$V_{i}$",);variable();
   line right 0.1
   resistor(right_ 0.5);variable()
   rlabel(,"$R_a$",);
   resistor(right_ 0.5);variable()
   rlabel(,"$R_b$",);
   {
     dot
     line down 0.5
     source(right_ 0.5,"$V_2$",);
     line up 0.5
     dot
   }
   resistor(right_ 0.5);variable()
   rlabel(,"$R_c$",);
   source(right_ 0.5,"A");
   line right 0.1 
   dot
   {
    source(down_ 0.8,"$V_1$",);
   }
   {
     move to Origin right 2.6 up 0.4
     right
     box ht 0.6 wid 0.8 fill(0.9)
     move to Origin right 2.8 up 1.3
   }

   line right 0.5
   diode(down_ 0.8);
   line left 0.5
   dot
   line to Origin
   dot
   ground()

   move to Origin right 3 
   resistor(up_ 0.8);rlabel(,"$R_{aq}$",);
   line right 0.8 
   battery(down_ 0.8,,R);llabel(,V_{aq},);variable();
   line left 0.8 
   dot
   ground
.PE
