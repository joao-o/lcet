reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'estacionario.tex'

# estacionario_1:15ºC
# estacionario_2:17.5ºC
# estacionario_3:20ºC(caso 1)
# estacionario_4:20ºC(caso 2)

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#4000bf' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#aa0055' lt 1 lw 1.5 pt 9 ps 1.5
set style line 4 lc rgb '#ff0000' lt 1 lw 1.5 pt 9 ps 1.5


# Axes
set style line 5 lc rgb '#000000' lt 1
set border 3 back ls 5
set tics nomirror out scale 0.5

# Grid
set style line 6 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 6


#Legenda
set key top right title 'Legenda' box 5

#axis labels
set xlabel '$x/cm$'
set ylabel '$T/^oC$' #rotate by  90 center 
set bars 2

lin1(x)=a1+b1*x
a1=0.1;b1=11;
fit lin1(x) 'estacionario_1.txt' using 1:4:5 yerror via a1,b1

lin2(x)=a2+b2*x
a2=0.1;b2=11;
fit lin2(x) 'estacionario_2.txt' using 1:4:5 yerror via a2,b2

lin3(x)=a3+b3*x
a3=0.1;b3=11;
fit lin3(x) 'estacionario_3.txt' using 1:4:5 yerror via a3,b3

lin4(x)=a4+b4*x
a4=0.1;b4=11;
fit lin4(x) 'estacionario_4.txt' using 1:4:5 yerror via a4,b4

#c=22;
#g(x)=c

set xrange [0:12] #mudar para 18 para observar magia
set yrange [20:100]

plot 'estacionario_1.txt' u 1:4:5 notitle w yerrorbars ls 1, \
     lin1(x) title sprintf('$T=(%1.2f\pm%0.2f)x+(%1.1f\pm%1.1f)$',b1,b1_err,a1,a1_err) ls 1, \
     'estacionario_2.txt' u 1:4:5 notitle w yerrorbars ls 2, \
     lin2(x) title sprintf('$T=(%1.2f\pm%0.2f)x+(%1.1f\pm%1.1f)$',b2,b2_err,a2,a2_err) ls 2, \
     'estacionario_3.txt' u 1:4:5 notitle w yerrorbars ls 3, \
     lin3(x) title sprintf('$T=(%1.2f\pm%0.2f)x+(%1.1f\pm%1.1f)$',b3,b3_err,a3,a3_err) ls 3, \
     'estacionario_4.txt' u 1:4:5 notitle w yerrorbars ls 4, \
     lin4(x) title sprintf('$T=(%1.2f\pm%0.2f)x+(%1.1f\pm%1.1f)$',b4,b4_err,a4,a4_err) ls 4
#     g(x) notitle
