
reset

# epslatex
set terminal epslatex size 14cm,10.5cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'x2_derivative.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 7 ps 1.7
set style line 2 lc rgb '#0000ff' lt 1 lw 1.5 pt 1 ps 1.5


# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12


#Legenda
set key top center title 'Legenda' box 5

#axis labels
set xlabel '$x/m$'
set ylabel '$T/^oC$' #rotate by  90 center 

#axis range
set yrange [31:45]
set xrange [0:0.12]
set bars 2

#fit linear
	quad(x)=a+b*x+c*x*x
	fit quad(x) "x2_derivative.txt" using 1:3 via a,b,c
	SSE= (FIT_WSSR)/(FIT_NDF)

#calculate R2
	mean(x)= m
	fit mean(x) 'x2_derivative.txt' using 1:3 via m
	SST = FIT_WSSR/(FIT_NDF+1)
	
	R2=(SST-SSE)/SST

plot 'x2_derivative.txt' u 1:3:2:4 notitle w xyerrorbars ls 1, \
      quad(x) title sprintf('$T=(%1.1f\pm%0.1f)x^2+(%1.1f\pm%1.1f)x+ (%1.1f\pm%1.1f)$',c,c_err,b,b_err,a,a_err) ls 2,\
      quad(x) title sprintf('$R^2=%f$',R2) ls 2
