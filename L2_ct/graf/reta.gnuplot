reset

do for [i=1:11]{
# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'reta_'.i.'.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#00ff00' lt 1 lw 1.5 pt 9 ps 1.5
set style line 4 lc rgb '#ff00ff' lt 1 lw 1.5 pt 9 ps 1.5
set style line 5 lc rgb '#ff0000' lt 1 lw 1.5 pt 9 ps 1.5
set style line 6 lc rgb '#0f000f' lt 1 lw 1.5 pt 9 ps 1.5
set style line 7 lc rgb '#00f00f' lt 1 lw 1.5 pt 9 ps 1.5
set style line 8 lc rgb '#f000ff' lt 1 lw 1.5 pt 9 ps 1.5
set style line 9 lc rgb '#0f00f0' lt 1 lw 1.5 pt 9 ps 1.5
set style line 10 lc rgb '#000f0f' lt 1 lw 1.5 pt 6 ps 1.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12


#Legenda
set key top left title 'Legenda' box 5

#axis labels
set xlabel '$t/s$'
set ylabel '$V/mL$' #rotate by  90 center 


set bars 2

#fit linear
	lin(x)=a+b*x
	a=0.1;b=11;
	fit lin(x) 'reta_'.i.'.txt' using 3:1:4:2 xyerror via a,b

#set label 1 '$log(U)=(5.31 \pm 0.05)\log(T)+(-15.6\pm 0.2)$' at 0.55,0.85 

#troquei os eixos para ficarem unidades não retardados de declive
plot 'reta_'.i.'.txt' u 3:1:4:2 notitle w xyerrorbars ls i, \
      lin(x) title sprintf('$V=(%1.3f\pm%0.3f)t+(%1.1f\pm%1.1f)$',b,b_err,a,a_err) ls i
}

#lin(x) title '$\\log(\\lambda)=(-1.3\\\pm0.3)\\log(T)+(-1.6\\\pm0.6)$' ls i
#sprintf("m = %3.4f",m)
