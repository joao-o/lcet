reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'naousar.tex'

# estacionario_1:15ºC
# estacionario_2:17.5ºC
# estacionario_3:20ºC(caso 1)
# estacionario_4:20ºC(caso 2)

#### color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#4000bf' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#aa0055' lt 1 lw 1.5 pt 9 ps 1.5
set style line 4 lc rgb '#ff0000' lt 1 lw 1.5 pt 9 ps 1.5
set style line 5 lc rgb '#ff00ff' lt 1 lw 1.5 pt 9 ps 1.5

#### Axes
set style line 6 lc rgb '#000000' lt 1
set border 3 back ls 6
set tics nomirror out scale 0.5

#### Grid
set style line 7 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 7


#### Legenda
set key top right title 'Legenda' box 5

#### axis labels
set xlabel '$x/cm$'
set ylabel '$T/^oC$' #rotate by  90 center 
set bars 2

#### plot ranges ####
#set xrange [0:14]
#set yrange [20:100]


plot 'estacionario_1.txt' u 1:4:5 notitle w yerrorbars ls 1



#lin1(x) title sprintf('$T=(%1.2f\pm%0.2f)x+(%1.1f\pm%1.1f)$',b1,b1_err,a1,a1_err) ls 1