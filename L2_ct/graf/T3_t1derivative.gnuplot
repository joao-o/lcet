
reset

# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 't1_derivative.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#ff0000' lt 1 lw 1.5 pt 10 ps 0.5
set style line 2 lc rgb '#000000' lt 1 lw 1.5 pt 6 ps 1.5


# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 12


#Legenda
set key at 195,43.5 title 'Legenda' box 5

#axis labelss
set xlabel '$t/s$'
set ylabel '$T/^oC$' #rotate by  90 center 

#axis range
set yrange [33:44]
set xrange [100:200]
set bars 2

#fit linear
	lin(x)=a+b*x
	fit lin(x) 'T3_t1derivative.txt' using 1:3 via a,b
	SSE= (FIT_WSSR)/(FIT_NDF)

#calculate R2
	mean(x)= m
	fit mean(x) 'T3_t1derivative.txt' using 1:3 via m
	SST = FIT_WSSR/(FIT_NDF+1)
	
	R2=(SST-SSE)/SST

plot 'T3_t1derivative.txt' u 1:3:2:4 notitle w xyerrorbars ls 1, \
      lin(x) title sprintf('$T=(%1.4f\pm%0.4f)t+(%1.1f\pm%1.1f)$',b,b_err,a,a_err) ls 2,\
      lin(x) title sprintf('$R^2=%f$',R2) ls 2
