reset

#epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'fonte_fria.tex'

#set terminal qt 

# color definitions
set border linewidth 1
set style line 1 lc rgb '#800000' lt 1 lw 1.5 pt 6 ps 1.5 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#0000ff' lt 1 lw 1.5 pt 6 ps 1.5
set style line 4 lc rgb '#ffa500' lt 1 lw 1.5 pt 6 ps 1.5
set style line 5 lc rgb '#9400d3' lt 1 lw 1.5 pt 6 ps 1.5
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

#set xrange [0:9]
#set xtics 1,1,9 #1st,step,last
#set logscale y
#set yrange [1e7:1e22]

#Legenda
#set key top right title '' box 5


#axis labels
set xlabel 't/ s'
set ylabel '$T_{FFria}/^oC$'

set tics scale 2

set bars 2 #espessura das barras de erro

plot 't_transitorio.txt' u 1:7:(0.125):(0.35) notitle ps 0.5  pt 3 lc rgb "#000000",\
     't_transitorio.txt' u 1:8:(0.125):(0.35) notitle ps 0.5  pt 3 lc rgb "#1832CF"
pause -1
