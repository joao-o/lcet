reset


# epslatex
set terminal epslatex size 12cm,9cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'retas.tex'

##### color definitions #####
set border linewidth 1.5
set style line 1 lc rgb '#283a40' lt 1 lw 1.5 pt 6 ps 1.5
set style line 2 lc rgb '#7aaec1' lt 1 lw 1.5 pt 6 ps 1.5
set style line 3 lc rgb '#324850' lt 1 lw 1.5 pt 9 ps 1.5
set style line 4 lc rgb '#3d5761' lt 1 lw 1.5 pt 9 ps 1.5
set style line 5 lc rgb '#84bdd2' lt 1 lw 1.5 pt 9 ps 1.5
set style line 6 lc rgb '#517481' lt 1 lw 1.5 pt 9 ps 1.5
set style line 7 lc rgb '#70a0b1' lt 1 lw 1.5 pt 9 ps 1.5
set style line 8 lc rgb '#6691a1' lt 1 lw 1.5 pt 9 ps 1.5
set style line 9 lc rgb '#1e2b30' lt 1 lw 1.5 pt 9 ps 1.5
set style line 10 lc rgb '#476571' lt 1 lw 1.5 pt 6 ps 1.5
set style line 11 lc rgb '#5b8291' lt 1 lw 1.5 pt 6 ps 1.5

##### Axes #####
set style line 12 lc rgb '#000000' lt 1
set border 3 back ls 12
set tics nomirror out scale 0.5
# Grid
set style line 13 dt 3 lc rgb'#808080' lt 1 lw 1 #dash type 3 - tracejado
set grid back ls 13


#### Legenda #####
#set key top left title 'Legenda' box 5
#não sei que legenda pôr


#axis labels
set xlabel '$t/s$'
set ylabel '$V/mL$' #rotate by  90 center 


#set bars 2

##### loops de funções e seus respetivos fits com macros #####
# define the functions depending on the current number
fstr(N) = sprintf("lin%d(x) = a%d +b%d*x",N,N,N)

# The fitting string for a specific file and the related function
fitstr(N) = sprintf("fit lin%d(x) 'reta_%d.txt' using 3:1:4:2 xyerror via a%d,b%d", N, N, N, N)

#do all the fitts at once
n=11
do for [i=1:n]{
	eval(fstr(i))	
	eval(fitstr(i))
}

#construct the complete plotting string
plotstr = "plot "

do for [i=1:n] {
    plotstr = plotstr . sprintf("lin%d(x) notitle ls %d, 'reta_%d.txt' u 3:1:4:2 notitle w xyerror ls %d%s", i, i, i, i, (i == n) ? "" : ", ")
}

#plotstr = "plot "
#do for [i=1:n] {
#   t = sprintf("lin%d(x) = %.2f*x + %.2f", i, value(sprintf('a%d', i)), value(sprintf('b%d', i)))
#   plotstr = plotstr . sprintf("lin%d(x) lt %d t '%s', ", i, i, t).\
#                       sprintf(" 'reta_%d.txt' lt %d %s ", i, i, (i == n) ? "" : ", ")
#}

eval(plotstr)
