reset

# epslatex para relatorio
set terminal epslatex size 16cm,14.5cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'transitorio_livre.tex'

# para poder manipular o grafico em 3D (correr no gnuplot5 no terminal este ficheiro se for esta a opção desejada)
# ainda se temd e descomentar o pause -1 no fim
# set terminal qt

set xrange[0:0.12]
set xlabel "x(m)"

set yrange[0:500]
set ylabel "t(s)"

set zrange[0:105]
set zlabel "T(ºC)"
set ticslevel 0

#x=0.085
Th=90;
Tc=20;
k=300;
L=0.12;       #free
l=0.12;
ro=2700;
c=904;
#set fit maxiter 18 prescale 

f(x,y)=Tc+(Th-Tc)*(8/(pi*pi))*(sum[n=0:10] (exp(-(k/(ro*c*L*L))*y*(pi/2+n*pi)*(pi/2+n*pi))*(((-1.0)**n)/((2*n+1)**2))*sin(((L-x)/L)*(pi/2+n*pi))))


fit f(x,y) 'transitorio_total.txt' using ($1*0.01):2:3:(0.125):(0.35) errors y,z via k,Th,Tc,L


set view 60,139
set key height 5
set key width -60
set key top right title 'Parâmetros obtidos' box 5



splot     't_transitorio.txt' using (0.01):1:2   notitle ps 0.5  pt 21 lc rgb "#FF0000",\
	  't_transitorio.txt' using (0.035):1:3  notitle ps 0.5  pt 21 lc rgb "#A20032",\
	  't_transitorio.txt' using (0.06):1:4   notitle ps 0.5  pt 21 lc rgb "#740086",\
	  't_transitorio.txt' using (0.085):1:5  notitle ps 0.5  pt 21 lc rgb "#1200AA",\
	  't_transitorio.txt' using (0.11):1:6   notitle ps 0.5  pt 21 lc rgb "#0000FF",\
	  f(x,y) title sprintf('\shortstack{$k=(%3.1f\pm%1.1f)~WK^{-1}m^{-1} $\\ $T_h=(%2.2f\pm%0.2f)~K $\\ $T_c=(%2.2f\pm%0.2f)~K $\\ $L=(%0.4f\pm%0.4f)~m$}',k,k_err,Th,Th_err,Tc,Tc_err,L,L_err)

#'t_transitorio.txt' using (0.146):1:7   notitle ps 0.5  pt 21 lc rgb "#000000",\

#splot 't_transitorio.txt' using (1):1:2   title "$1~cm$"   ps 0.5  pt 21 lc rgb "#FF0000",\
#	  't_transitorio.txt' using (3.5):1:3 title "$3.5~cm$" ps 0.5  pt 21 lc rgb "#A20032",\
#	  't_transitorio.txt' using (6):1:4   title "$6~cm$"   ps 0.5  pt 21 lc rgb "#740086",\
#	  't_transitorio.txt' using (8.5):1:5 title "$8.5~cm$" ps 0.5  pt 21 lc rgb "#1200AA",\
#	  't_transitorio.txt' using (11):1:6  title "$11~cm$"  ps 0.5  pt 21 lc rgb "#0000FF"

#pause -1  #enable for tests
